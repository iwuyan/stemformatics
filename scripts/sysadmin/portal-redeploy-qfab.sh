#!/bin/bash

export PORTAL_REDEPLOY="echo \"Updating repository..\"; cd /data/repo/mercurial/ASCC; hg pull -u; echo \"Re-deploying..\"; cd scripts; ./portal-admin -c QFAB dev deploy"

if [ `whoami` != "portaladmin" ]; then
  echo "Please auth as 'portaladmin'."
  su portaladmin -m -c "export USER=\"portaladmin\"; export HOME=\"/home/portaladmin\"; eval $PORTAL_REDEPLOY"
else
  eval $PORTAL_REDEPLOY
fi
cd $OLDPWD
