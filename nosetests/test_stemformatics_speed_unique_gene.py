import logging
log = logging.getLogger(__name__)


import sqlalchemy as SA
from sqlalchemy import or_, and_, desc

import re
import string
import json

from guide.model.stemformatics import *
from guide.model.tfv import *

from guide.lib.state import *

useSqlSoup = True

mouse_db_id = 46
human_db_id = 56

""" Testing the gene controller"""
baseDir = '/home/s2776403/'

import random


def test_get_unique_gene_success_human_single_unique():
    
    
    db_id = 56
    
    
    """
        Setting up a pretend mapping - just for testing purposes
    """
    
    f_probe_list = baseDir + 'map_genes.txt'
    
    f_gl = open(f_probe_list,'r')
    
    probe_mapping = {}
    probe_mapping[db_id] = {}
    
    probe_list = []
    
    for line in f_gl:
        probe = line.replace('\n','')
        probe_mapping[db_id][probe] = [('ENSG' + str(random.randint(0,100000)),'STAT'),('ENSG' + str(random.randint(0,100000)),'NEW')]
        probe_list.append(probe)
    
    # print probe_mapping[db_id]['ILMN_1693766']
    
    probe_list.append('ILMN_158')
    
    """
        Finished setting up pretend mapping 0.063 seconds for 10,000 probes with two ensembl ids each
    """
    
    
    
    
    # m = re.findall('[\w\.\-\@]{2,}',geneSetRaw)
        
    # now input this list into a gene function that will return a dictionary
    # { 'ILMN_2174394' : { 'original' : 'ILMN_2174394', 'symbol' : 'STAT1', 'status' : 'OK' } }
    # If we make it a list of objects then we can sort, we cannot sort on a dictionary
    resultData = Stemformatics_Gene.get_unique_gene_fast(db,probe_list,db_id,'probe',probe_mapping)
    
    # print resultData['ILMN_1710209']
    
    assert resultData['ILMN_1710209']['status'] == 'Ambiguous'
    assert resultData['ILMN_1710209']['symbol'] == ['STAT','NEW']
    assert resultData['ILMN_1710209']['original'] == 'ILMN_1710209'


    print resultData['ILMN_158']
    assert resultData['ILMN_158']['status'] == 'Not found'
