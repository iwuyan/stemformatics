"""

    nosetests --with-pylons=config.ini guide/model/stemformatics/test_stemformatics_dataset.py


"""

from guide.model.stemformatics import *

from pylons import config
def test_ds_id_summary():
    msc_set = 'All'
    result = Stemformatics_Msc_Signature.get_msc_samples_summary_by_ds_id(db,msc_set)
    print result

def test_access():
    uid = 44
    access = Stemformatics_Msc_Signature.get_user_access(db,uid)
    assert access == False

    uid = 3
    access = Stemformatics_Msc_Signature.get_user_access(db,uid)
    assert access == True

    uid = 0
    access = Stemformatics_Msc_Signature.get_user_access(db,uid)
    assert access == False

    uid = 199
    access = Stemformatics_Msc_Signature.get_user_access(db,uid)
    assert access == False
 
 
def test_all_msc():
    msc_set = 'Rejected'
    msc_set = 'Training'
    msc_set = 'Testing'
    msc_set = 'All'



    returnData = Stemformatics_Msc_Signature.get_msc_samples(db,msc_set)
    print returnData
    print len(returnData[3000])

    assert returnData[3000]['4380071023_F']['Replicate Group ID'] == 'fibroblast_GSM425439'
    assert returnData[3000]['4380071023_F']['Sample Type'] == 'fibroblast enriched stromal (CD49f- EpCAM-)'

