import logging
log = logging.getLogger(__name__)


import sqlalchemy as SA
from sqlalchemy import or_, and_, desc

import re
import string
import json

from guide.model.stemformatics import *

from guide.lib.state import *

useSqlSoup = True

mouse_db_id = 46
human_db_id = 56
species_dict = Stemformatics_Gene.get_species(db)

""" Testing the gene controller"""

# -------------------------------------- Test pre_gene_search ---------------------------------------------------

def test_pre_gene_search_symbols_and():
    geneSearchFinal = Stemformatics_Gene._preGeneSearch(' $#$#@    CDK11  ,*^   STAT, pinch-3')
    print geneSearchFinal
    assert geneSearchFinal == '@&CDK11&STAT&pinch-3'

def test_pre_gene_search_symbols_or():
    geneSearchFinal = Stemformatics_Gene._preGeneSearch(' $#$#@    CDK11,*^STAT ,pinch-3')
    print geneSearchFinal
    assert geneSearchFinal == '@&CDK11|STAT&pinch-3'


def test_pre_gene_search_symbols_crazy():
    geneSearchFinal = Stemformatics_Gene._preGeneSearch(' CDK11&^^&^&^*23423STAT4234')
    print geneSearchFinal
    assert geneSearchFinal == 'CDK11&23423STAT4234'

def test_pre_gene_search_symbols_crazier():
    geneSearchFinal = Stemformatics_Gene._preGeneSearch(' CDK11^^|^^*23423(^%$#@$@$STAT4234^^^$$$###|ENSG00000166523')
    print geneSearchFinal
    assert geneSearchFinal == 'CDK11|23423|@|@|STAT4234|ENSG00000166523'


def test_pre_gene_search_sql_injection():
    geneSearchFinal = Stemformatics_Gene._preGeneSearch(' CDK11;drop probe_expressions;')
    print geneSearchFinal
    assert geneSearchFinal == 'CDK11|drop&probe_expressions'



def test_pre_gene_search_basic():
    geneSearchFinal = Stemformatics_Gene._preGeneSearch('CDK11')
    print geneSearchFinal
    assert geneSearchFinal == 'CDK11'

""" Check spaces turn into & for and"""
def test_pre_gene_search_spaces():
    geneSearchFinal = Stemformatics_Gene._preGeneSearch('CDK11 STAT1 STAT5')
    print geneSearchFinal
    assert geneSearchFinal == 'CDK11&STAT1&STAT5'

""" Check commas turn into | for or"""
def test_pre_gene_search_comma():
    geneSearchFinal = Stemformatics_Gene._preGeneSearch('CDK11,STAT1,STAT5')
    print geneSearchFinal
    assert geneSearchFinal == 'CDK11|STAT1|STAT5'


def test_pre_gene_search_pipe():
    geneSearchFinal = Stemformatics_Gene._preGeneSearch('CDK11|STAT1|STAT5')
    print geneSearchFinal
    assert geneSearchFinal == 'CDK11|STAT1|STAT5'

def test_pre_gene_search_ampasand():
    geneSearchFinal = Stemformatics_Gene._preGeneSearch('CDK11&STAT1&STAT5')
    print geneSearchFinal
    assert geneSearchFinal == 'CDK11&STAT1&STAT5'
    
# -------------------------------------- Test getAutoComplete ---------------------------------------------------

def test_gene_autocomplete_sql_injection_with_human():
    
    explicit_search = None # not used
    max_number = 10000
    gene_search = 'CDK11;drop probe_expressions;'
    returnData = Stemformatics_Gene.getAutoComplete(db,species_dict,gene_search,human_db_id,explicit_search,max_number)
    print returnData
    assert returnData == [{'ensembl_id': u'ENSG00000008128', 'db_id': 56, 'description': u'cyclin-dependent kinase 11A <br />', 'symbol': u'CDK11A', 'species': u'Homo sapiens', 'aliases': u'CDC2L2 CDC2L3 CDK11-p110 CDK11-p46 CDK11-p58 PITSLRE p58GTA'}, {'ensembl_id': u'ENSG00000248333', 'db_id': 56, 'description': u'cyclin-dependent kinase 11B <br />', 'symbol': u'CDK11B', 'species': u'Homo sapiens', 'aliases': u'CDC2L1 CDK11 CDK11-p110 CDK11-p46 CDK11-p58 CLK-1 PK58 p58 p58CDC2L1 p58CLK-1'}, {'ensembl_id': u'ENSG00000155111', 'db_id': 56, 'description': u'cyclin-dependent kinase 19 <br />', 'symbol': u'CDK19', 'species': u'Homo sapiens', 'aliases': u'CDC2L6 CDK11 bA346C16.3'}]


def test_gene_autocomplete_symbols_with_human():
    explicit_search = None # not used
    max_number = 10000
    gene_search = ' CDK11^^|^^*23423(^%$#@$@$STAT4234^^^$$$###|ENSG00000166523'
    db_id = human_db_id
    returnData = Stemformatics_Gene.getAutoComplete(db,species_dict,gene_search,db_id,explicit_search,max_number)
    print returnData
    assert returnData == [{'ensembl_id': u'ENSG00000008128', 'db_id': 56, 'description': u'cyclin-dependent kinase 11A <br />', 'symbol': u'CDK11A', 'species': u'Homo sapiens', 'aliases': u'CDC2L2 CDC2L3 CDK11-p110 CDK11-p46 CDK11-p58 PITSLRE p58GTA'}, {'ensembl_id': u'ENSG00000248333', 'db_id': 56, 'description': u'cyclin-dependent kinase 11B <br />', 'symbol': u'CDK11B', 'species': u'Homo sapiens', 'aliases': u'CDC2L1 CDK11 CDK11-p110 CDK11-p46 CDK11-p58 CLK-1 PK58 p58 p58CDC2L1 p58CLK-1'}, {'ensembl_id': u'ENSG00000155111', 'db_id': 56, 'description': u'cyclin-dependent kinase 19 <br />', 'symbol': u'CDK19', 'species': u'Homo sapiens', 'aliases': u'CDC2L6 CDK11 bA346C16.3'}, {'ensembl_id': u'ENSG00000166523', 'db_id': 56, 'description': u'C-type lectin domain family 4, member E <br />', 'symbol': u'CLEC4E', 'species': u'Homo sapiens', 'aliases': u'CLECSF9 MINCLE'}, {'ensembl_id': u'ENSG00000183675', 'db_id': 56, 'description': u'protein tyrosine phosphatase, non-receptor type 20B <br />', 'symbol': u'PTPN20B', 'species': u'Homo sapiens', 'aliases': u'bA42B19.1'}, {'ensembl_id': u'ENSG00000264832', 'db_id': 56, 'description': u'protein tyrosine phosphatase, non-receptor type 20B <br />', 'symbol': u'PTPN20B', 'species': u'Homo sapiens', 'aliases': u'bA42B19.1'}, {'ensembl_id': u'ENSG00000166557', 'db_id': 56, 'description': u'transmembrane emp24 protein transport domain containing 3 <br />', 'symbol': u'TMED3', 'species': u'Homo sapiens', 'aliases': u'C15orf22 P24B p26'}]


def test_gene_autocomplete_ensg_with_human():
    explicit_search = None # not used
    max_number = 10000
    gene_search = 'ENSG00000166523'
    db_id = human_db_id
    returnData = Stemformatics_Gene.getAutoComplete(db,species_dict,gene_search,db_id,explicit_search,max_number)
    print returnData
    assert returnData == [{'ensembl_id': u'ENSG00000166523', 'db_id': 56, 'description': u'C-type lectin domain family 4, member E <br />', 'symbol': u'CLEC4E', 'species': u'Homo sapiens', 'aliases': u'CLECSF9 MINCLE'}]


def test_gene_autocomplete_stat_with_human():
    explicit_search = None # not used
    max_number = 10000
    gene_search = 'stat'
    db_id = human_db_id
    returnData = Stemformatics_Gene.getAutoComplete(db,species_dict,gene_search,db_id,explicit_search,max_number)
    print returnData
    assert returnData == [{'ensembl_id': u'ENSG00000115415', 'db_id': 56, 'description': u'signal transducer and activator of transcription 1, 91kDa <br />', 'symbol': u'STAT1', 'species': u'Homo sapiens', 'aliases': u'CANDF7 ISGF-3 STAT91'}, {'ensembl_id': u'ENSG00000170581', 'db_id': 56, 'description': u'signal transducer and activator of transcription 2, 113kDa <br />', 'symbol': u'STAT2', 'species': u'Homo sapiens', 'aliases': u'ISGF-3 P113 STAT113'}, {'ensembl_id': u'ENSG00000168610', 'db_id': 56, 'description': u'signal transducer and activator of transcription 3 (acute-phase response factor) <br />', 'symbol': u'STAT3', 'species': u'Homo sapiens', 'aliases': u'APRF HIES'}, {'ensembl_id': u'ENSG00000138378', 'db_id': 56, 'description': u'signal transducer and activator of transcription 4 <br />', 'symbol': u'STAT4', 'species': u'Homo sapiens', 'aliases': u'SLEB11'}, {'ensembl_id': u'ENSG00000126561', 'db_id': 56, 'description': u'signal transducer and activator of transcription 5A <br />', 'symbol': u'STAT5A', 'species': u'Homo sapiens', 'aliases': u'MGF STAT5'}, {'ensembl_id': u'ENSG00000173757', 'db_id': 56, 'description': u'signal transducer and activator of transcription 5B <br />', 'symbol': u'STAT5B', 'species': u'Homo sapiens', 'aliases': u'STAT5'}, {'ensembl_id': u'ENSG00000166888', 'db_id': 56, 'description': u'signal transducer and activator of transcription 6, interleukin-4 induced <br />', 'symbol': u'STAT6', 'species': u'Homo sapiens', 'aliases': u'D12S1644 IL-4-STAT STAT6B STAT6C'}, {'ensembl_id': u'ENSG00000126549', 'db_id': 56, 'description': u'statherin <br />', 'symbol': u'STATH', 'species': u'Homo sapiens', 'aliases': u'STR'}, {'ensembl_id': u'ENSG00000172889', 'db_id': 56, 'description': u'EGF-like-domain, multiple 7 <br />', 'symbol': u'EGFL7', 'species': u'Homo sapiens', 'aliases': u'NEU1 RP11-251M1.2 VE-STATIN ZNEU1'}, {'ensembl_id': u'ENSG00000134759', 'db_id': 56, 'description': u'elongator acetyltransferase complex subunit 2 <br />', 'symbol': u'ELP2', 'species': u'Homo sapiens', 'aliases': u'SHINC-2 STATIP1 StIP'}, {'ensembl_id': u'ENSG00000123609', 'db_id': 56, 'description': u'N-myc (and STAT) interactor <br />', 'symbol': u'NMI', 'species': u'Homo sapiens', 'aliases': u''}, {'ensembl_id': u'ENSG00000033800', 'db_id': 56, 'description': u'protein inhibitor of activated STAT, 1 <br />', 'symbol': u'PIAS1', 'species': u'Homo sapiens', 'aliases': u'DDXBP1 GBP GU/RH-II ZMIZ3'}, {'ensembl_id': u'ENSG00000078043', 'db_id': 56, 'description': u'protein inhibitor of activated STAT, 2 <br />', 'symbol': u'PIAS2', 'species': u'Homo sapiens', 'aliases': u'ARIP3 DIP MIZ MIZ1 PIASX PIASX-ALPHA PIASX-BETA SIZ2 ZMIZ4'}, {'ensembl_id': u'ENSG00000131788', 'db_id': 56, 'description': u'protein inhibitor of activated STAT, 3 <br />', 'symbol': u'PIAS3', 'species': u'Homo sapiens', 'aliases': u'ZMIZ5'}, {'ensembl_id': u'ENSG00000263461', 'db_id': 56, 'description': u'protein inhibitor of activated STAT, 3 <br />', 'symbol': u'PIAS3', 'species': u'Homo sapiens', 'aliases': u'ZMIZ5'}, {'ensembl_id': u'ENSG00000105229', 'db_id': 56, 'description': u'protein inhibitor of activated STAT, 4 <br />', 'symbol': u'PIAS4', 'species': u'Homo sapiens', 'aliases': u'PIASY Piasg ZMIZ6'}, {'ensembl_id': u'ENSG00000057252', 'db_id': 56, 'description': u'sterol O-acyltransferase 1 <br />', 'symbol': u'SOAT1', 'species': u'Homo sapiens', 'aliases': u'ACACT ACAT ACAT-1 ACAT1 SOAT STAT'}, {'ensembl_id': u'ENSG00000120833', 'db_id': 56, 'description': u'suppressor of cytokine signaling 2 <br />', 'symbol': u'SOCS2', 'species': u'Homo sapiens', 'aliases': u'CIS2 Cish2 SOCS-2 SSI-2 SSI2 STATI2'}, {'ensembl_id': u'ENSG00000170677', 'db_id': 56, 'description': u'suppressor of cytokine signaling 6 <br />', 'symbol': u'SOCS6', 'species': u'Homo sapiens', 'aliases': u'CIS-4 CIS4 HSPC060 SOCS-4 SOCS-6 SOCS4 SSI4 STAI4 STATI4'}]


def test_gene_autocomplete_stat_with_mouse():
    explicit_search = None # not used
    max_number = 10000
    gene_search = 'stat'
    db_id = mouse_db_id
    returnData = Stemformatics_Gene.getAutoComplete(db,species_dict,gene_search,db_id,explicit_search,max_number)
    print returnData
    assert returnData == [{'ensembl_id': u'ENSMUSG00000026104', 'db_id': 46, 'description': u'signal transducer and activator of transcription 1 <br />', 'symbol': u'Stat1', 'species': u'Mus musculus', 'aliases': u'2010005J02Rik AA408197'}, {'ensembl_id': u'ENSMUSG00000040033', 'db_id': 46, 'description': u'signal transducer and activator of transcription 2 <br />', 'symbol': u'Stat2', 'species': u'Mus musculus', 'aliases': u'1600010G07Rik AW496480'}, {'ensembl_id': u'ENSMUSG00000004040', 'db_id': 46, 'description': u'signal transducer and activator of transcription 3 <br />', 'symbol': u'Stat3', 'species': u'Mus musculus', 'aliases': u'1110034C02Rik AW109958 Aprf'}, {'ensembl_id': u'ENSMUSG00000062939', 'db_id': 46, 'description': u'signal transducer and activator of transcription 4 <br />', 'symbol': u'Stat4', 'species': u'Mus musculus', 'aliases': u''}, {'ensembl_id': u'ENSMUSG00000004043', 'db_id': 46, 'description': u'signal transducer and activator of transcription 5A <br />', 'symbol': u'Stat5a', 'species': u'Mus musculus', 'aliases': u'AA959963 STAT5'}, {'ensembl_id': u'ENSMUSG00000020919', 'db_id': 46, 'description': u'signal transducer and activator of transcription 5B <br />', 'symbol': u'Stat5b', 'species': u'Mus musculus', 'aliases': u''}, {'ensembl_id': u'ENSMUSG00000002147', 'db_id': 46, 'description': u'signal transducer and activator of transcription 6 <br />', 'symbol': u'Stat6', 'species': u'Mus musculus', 'aliases': u''}, {'ensembl_id': u'ENSMUSG00000024271', 'db_id': 46, 'description': u'elongation protein 2 homolog (S. cerevisiae) <br />', 'symbol': u'Elp2', 'species': u'Mus musculus', 'aliases': u'AU023723 Epl2 StIP1 Statip1'}, {'ensembl_id': u'ENSMUSG00000026946', 'db_id': 46, 'description': u'N-myc (and STAT) interactor <br />', 'symbol': u'Nmi', 'species': u'Mus musculus', 'aliases': u''}, {'ensembl_id': u'ENSMUSG00000032405', 'db_id': 46, 'description': u'protein inhibitor of activated STAT 1 <br />', 'symbol': u'Pias1', 'species': u'Mus musculus', 'aliases': u'2900068C24Rik Ddxbp1 GBP'}, {'ensembl_id': u'ENSMUSG00000025423', 'db_id': 46, 'description': u'protein inhibitor of activated STAT 2 <br />', 'symbol': u'Pias2', 'species': u'Mus musculus', 'aliases': u'6330408K17Rik AI462206 ARIP3 AU018068 Dib Miz1 PIASxalpha PIASxb PIASxbeta SIZ2'}, {'ensembl_id': u'ENSMUSG00000028101', 'db_id': 46, 'description': u'protein inhibitor of activated STAT 3 <br />', 'symbol': u'Pias3', 'species': u'Mus musculus', 'aliases': u''}, {'ensembl_id': u'ENSMUSG00000004934', 'db_id': 46, 'description': u'protein inhibitor of activated STAT 4 <br />', 'symbol': u'Pias4', 'species': u'Mus musculus', 'aliases': u'PIASY Pias-gamma Piasg'}, {'ensembl_id': u'ENSMUSG00000056153', 'db_id': 46, 'description': u'suppressor of cytokine signaling 6 <br />', 'symbol': u'Socs6', 'species': u'Mus musculus', 'aliases': u'1500012M23Rik 5830401B18Rik AI447482 Cis4 Cish4 HSPC060 SOCS-4 SOCS-6 SSI4 STAI4 STATI4 Socs4'}]


def test_gene_autocomplete_stat_all():
    explicit_search = None # not used
    max_number = 10000
    gene_search = 'stat'
    db_id = None
    returnData = Stemformatics_Gene.getAutoComplete(db,species_dict,gene_search,db_id,explicit_search,max_number)
    print returnData
    assert returnData == [{'ensembl_id': u'ENSG00000115415', 'db_id': 56, 'description': u'signal transducer and activator of transcription 1, 91kDa <br />', 'symbol': u'STAT1', 'species': u'Homo sapiens', 'aliases': u'CANDF7 ISGF-3 STAT91'}, {'ensembl_id': u'ENSMUSG00000026104', 'db_id': 46, 'description': u'signal transducer and activator of transcription 1 <br />', 'symbol': u'Stat1', 'species': u'Mus musculus', 'aliases': u'2010005J02Rik AA408197'}, {'ensembl_id': u'ENSG00000170581', 'db_id': 56, 'description': u'signal transducer and activator of transcription 2, 113kDa <br />', 'symbol': u'STAT2', 'species': u'Homo sapiens', 'aliases': u'ISGF-3 P113 STAT113'}, {'ensembl_id': u'ENSMUSG00000040033', 'db_id': 46, 'description': u'signal transducer and activator of transcription 2 <br />', 'symbol': u'Stat2', 'species': u'Mus musculus', 'aliases': u'1600010G07Rik AW496480'}, {'ensembl_id': u'ENSG00000168610', 'db_id': 56, 'description': u'signal transducer and activator of transcription 3 (acute-phase response factor) <br />', 'symbol': u'STAT3', 'species': u'Homo sapiens', 'aliases': u'APRF HIES'}, {'ensembl_id': u'ENSMUSG00000004040', 'db_id': 46, 'description': u'signal transducer and activator of transcription 3 <br />', 'symbol': u'Stat3', 'species': u'Mus musculus', 'aliases': u'1110034C02Rik AW109958 Aprf'}, {'ensembl_id': u'ENSG00000138378', 'db_id': 56, 'description': u'signal transducer and activator of transcription 4 <br />', 'symbol': u'STAT4', 'species': u'Homo sapiens', 'aliases': u'SLEB11'}, {'ensembl_id': u'ENSMUSG00000062939', 'db_id': 46, 'description': u'signal transducer and activator of transcription 4 <br />', 'symbol': u'Stat4', 'species': u'Mus musculus', 'aliases': u''}, {'ensembl_id': u'ENSG00000126561', 'db_id': 56, 'description': u'signal transducer and activator of transcription 5A <br />', 'symbol': u'STAT5A', 'species': u'Homo sapiens', 'aliases': u'MGF STAT5'}, {'ensembl_id': u'ENSMUSG00000004043', 'db_id': 46, 'description': u'signal transducer and activator of transcription 5A <br />', 'symbol': u'Stat5a', 'species': u'Mus musculus', 'aliases': u'AA959963 STAT5'}, {'ensembl_id': u'ENSG00000173757', 'db_id': 56, 'description': u'signal transducer and activator of transcription 5B <br />', 'symbol': u'STAT5B', 'species': u'Homo sapiens', 'aliases': u'STAT5'}, {'ensembl_id': u'ENSMUSG00000020919', 'db_id': 46, 'description': u'signal transducer and activator of transcription 5B <br />', 'symbol': u'Stat5b', 'species': u'Mus musculus', 'aliases': u''}, {'ensembl_id': u'ENSG00000166888', 'db_id': 56, 'description': u'signal transducer and activator of transcription 6, interleukin-4 induced <br />', 'symbol': u'STAT6', 'species': u'Homo sapiens', 'aliases': u'D12S1644 IL-4-STAT STAT6B STAT6C'}, {'ensembl_id': u'ENSMUSG00000002147', 'db_id': 46, 'description': u'signal transducer and activator of transcription 6 <br />', 'symbol': u'Stat6', 'species': u'Mus musculus', 'aliases': u''}, {'ensembl_id': u'ENSG00000126549', 'db_id': 56, 'description': u'statherin <br />', 'symbol': u'STATH', 'species': u'Homo sapiens', 'aliases': u'STR'}, {'ensembl_id': u'ENSG00000172889', 'db_id': 56, 'description': u'EGF-like-domain, multiple 7 <br />', 'symbol': u'EGFL7', 'species': u'Homo sapiens', 'aliases': u'NEU1 RP11-251M1.2 VE-STATIN ZNEU1'}, {'ensembl_id': u'ENSG00000134759', 'db_id': 56, 'description': u'elongator acetyltransferase complex subunit 2 <br />', 'symbol': u'ELP2', 'species': u'Homo sapiens', 'aliases': u'SHINC-2 STATIP1 StIP'}, {'ensembl_id': u'ENSMUSG00000024271', 'db_id': 46, 'description': u'elongation protein 2 homolog (S. cerevisiae) <br />', 'symbol': u'Elp2', 'species': u'Mus musculus', 'aliases': u'AU023723 Epl2 StIP1 Statip1'}, {'ensembl_id': u'ENSG00000123609', 'db_id': 56, 'description': u'N-myc (and STAT) interactor <br />', 'symbol': u'NMI', 'species': u'Homo sapiens', 'aliases': u''}, {'ensembl_id': u'ENSMUSG00000026946', 'db_id': 46, 'description': u'N-myc (and STAT) interactor <br />', 'symbol': u'Nmi', 'species': u'Mus musculus', 'aliases': u''}] 

""" Should max out at 20, stat has 33 records associated with it"""
def test_gene_autocomplete_stat_all_length():
    explicit_search = None # not used
    max_number = 10000
    gene_search = 'stat'
    db_id = None
    returnData = Stemformatics_Gene.getAutoComplete(db,species_dict,gene_search,db_id,explicit_search,max_number)
    print len(returnData)
    assert len(returnData) == 20
    
def test_gene_autocomplete_stat1_all():
    explicit_search = None # not used
    max_number = 10000
    gene_search = 'stat1'
    db_id = None
    returnData = Stemformatics_Gene.getAutoComplete(db,species_dict,gene_search,db_id,explicit_search,max_number)
    print returnData
    assert returnData == [{'ensembl_id': u'ENSG00000115415', 'db_id': 56, 'description': u'signal transducer and activator of transcription 1, 91kDa <br />', 'symbol': u'STAT1', 'species': u'Homo sapiens', 'aliases': u'CANDF7 ISGF-3 STAT91'}, {'ensembl_id': u'ENSMUSG00000026104', 'db_id': 46, 'description': u'signal transducer and activator of transcription 1 <br />', 'symbol': u'Stat1', 'species': u'Mus musculus', 'aliases': u'2010005J02Rik AA408197'}, {'ensembl_id': u'ENSG00000170581', 'db_id': 56, 'description': u'signal transducer and activator of transcription 2, 113kDa <br />', 'symbol': u'STAT2', 'species': u'Homo sapiens', 'aliases': u'ISGF-3 P113 STAT113'}]

def test_gene_autocomplete_cdk11_all():
    explicit_search = None # not used
    max_number = 10000
    gene_search = 'cdk11'
    db_id = None
    returnData = Stemformatics_Gene.getAutoComplete(db,species_dict,gene_search,db_id,explicit_search,max_number)
    print returnData
    assert returnData == [{'ensembl_id': u'ENSG00000008128', 'db_id': 56, 'description': u'cyclin-dependent kinase 11A <br />', 'symbol': u'CDK11A', 'species': u'Homo sapiens', 'aliases': u'CDC2L2 CDC2L3 CDK11-p110 CDK11-p46 CDK11-p58 PITSLRE p58GTA'}, {'ensembl_id': u'ENSG00000248333', 'db_id': 56, 'description': u'cyclin-dependent kinase 11B <br />', 'symbol': u'CDK11B', 'species': u'Homo sapiens', 'aliases': u'CDC2L1 CDK11 CDK11-p110 CDK11-p46 CDK11-p58 CLK-1 PK58 p58 p58CDC2L1 p58CLK-1'}, {'ensembl_id': u'ENSMUSG00000029062', 'db_id': 46, 'description': u'cyclin-dependent kinase 11B <br />', 'symbol': u'Cdk11b', 'species': u'Mus musculus', 'aliases': u'AA989746 CDK11-p110 CDK11-p46 CDK11-p58 Cdc11b Cdc2l1 Cdc2l2 Cdk11 p58'}, {'ensembl_id': u'ENSG00000155111', 'db_id': 56, 'description': u'cyclin-dependent kinase 19 <br />', 'symbol': u'CDK19', 'species': u'Homo sapiens', 'aliases': u'CDC2L6 CDK11 bA346C16.3'}, {'ensembl_id': u'ENSMUSG00000038481', 'db_id': 46, 'description': u'cyclin-dependent kinase 19 <br />', 'symbol': u'Cdk19', 'species': u'Mus musculus', 'aliases': u'2700084L06Rik AI845279 AW228747 CDK11 Cdc2l6 mKIAA1028'}]


def test_gene_autocomplete_mincle_all():
    explicit_search = None # not used
    max_number = 10000
    gene_search = 'mincle'
    db_id = None
    returnData = Stemformatics_Gene.getAutoComplete(db,species_dict,gene_search,db_id,explicit_search,max_number)
    print returnData
    assert returnData == [{'ensembl_id': u'ENSG00000166523', 'db_id': 56, 'description': u'C-type lectin domain family 4, member E <br />', 'symbol': u'CLEC4E', 'species': u'Homo sapiens', 'aliases': u'CLECSF9 MINCLE'}, {'ensembl_id': u'ENSMUSG00000030142', 'db_id': 46, 'description': u'C-type lectin domain family 4, member e <br />', 'symbol': u'Clec4e', 'species': u'Mus musculus', 'aliases': u'C86253 Clecsf9 Mincle'}]


def test_gene_autocomplete_kinase_all():
    explicit_search = None # not used
    max_number = 10000
    gene_search = 'kinase'
    db_id = None
    returnData = Stemformatics_Gene.getAutoComplete(db,species_dict,gene_search,db_id,explicit_search,max_number)
    print returnData
    assert returnData == [{'ensembl_id': u'ENSG00000115977', 'db_id': 56, 'description': u'AP2 associated kinase 1 <br />', 'symbol': u'AAK1', 'species': u'Homo sapiens', 'aliases': u''}, {'ensembl_id': u'ENSMUSG00000057230', 'db_id': 46, 'description': u'AP2 associated kinase 1 <br />', 'symbol': u'Aak1', 'species': u'Mus musculus', 'aliases': u''}, {'ensembl_id': u'ENSG00000181409', 'db_id': 56, 'description': u'apoptosis-associated tyrosine kinase <br />', 'symbol': u'AATK', 'species': u'Homo sapiens', 'aliases': u'AATYK AATYK1 LMR1 LMTK1 p35BP'}, {'ensembl_id': u'ENSMUSG00000025375', 'db_id': 46, 'description': u'apoptosis-associated tyrosine kinase <br />', 'symbol': u'Aatk', 'species': u'Mus musculus', 'aliases': u'AATYK aatyk1 mKIAA0641'}, {'ensembl_id': u'ENSG00000097007', 'db_id': 56, 'description': u'c-abl oncogene 1, non-receptor tyrosine kinase <br />', 'symbol': u'ABL1', 'species': u'Homo sapiens', 'aliases': u'ABL JTK7 bcr/abl c-ABL p150 v-abl'}, {'ensembl_id': u'ENSMUSG00000026842', 'db_id': 46, 'description': u'c-abl oncogene 1, non-receptor tyrosine kinase <br />', 'symbol': u'Abl1', 'species': u'Mus musculus', 'aliases': u'AI325092 Abl E430008G22Rik c-Abl'}, {'ensembl_id': u'ENSMUSG00000090901', 'db_id': 46, 'description': u'c-abl oncogene 1, non-receptor tyrosine kinase <br />', 'symbol': u'Abl1', 'species': u'Mus musculus', 'aliases': u''}, {'ensembl_id': u'ENSG00000091436', 'db_id': 56, 'description': u'Mitogen-activated protein kinase kinase kinase MLT  <br />', 'symbol': u'AC013461.1', 'species': u'Homo sapiens', 'aliases': u'AZK MLK7 MLT MLTK MRK mlklak pk'}, {'ensembl_id': u'ENSG00000177453', 'db_id': 56, 'description': u'Serine/threonine-protein kinase NIM1  <br />', 'symbol': u'AC114947.1', 'species': u'Homo sapiens', 'aliases': u''}, {'ensembl_id': u'ENSG00000063761', 'db_id': 56, 'description': u'aarF domain containing kinase 1 <br />', 'symbol': u'ADCK1', 'species': u'Homo sapiens', 'aliases': u''}, {'ensembl_id': u'ENSMUSG00000021044', 'db_id': 46, 'description': u'aarF domain containing kinase 1 <br />', 'symbol': u'Adck1', 'species': u'Mus musculus', 'aliases': u'2610005A10Rik'}, {'ensembl_id': u'ENSG00000133597', 'db_id': 56, 'description': u'aarF domain containing kinase 2 <br />', 'symbol': u'ADCK2', 'species': u'Homo sapiens', 'aliases': u'AARF'}, {'ensembl_id': u'ENSMUSG00000046947', 'db_id': 46, 'description': u'aarF domain containing kinase 2 <br />', 'symbol': u'Adck2', 'species': u'Mus musculus', 'aliases': u'AA408112 BB006057'}, {'ensembl_id': u'ENSG00000163050', 'db_id': 56, 'description': u'aarF domain containing kinase 3 <br />', 'symbol': u'ADCK3', 'species': u'Homo sapiens', 'aliases': u'ARCA2 CABC1 COQ10D4 COQ8 SCAR9'}, {'ensembl_id': u'ENSMUSG00000026489', 'db_id': 46, 'description': u'aarF domain containing kinase 3 <br />', 'symbol': u'Adck3', 'species': u'Mus musculus', 'aliases': u'4632432J16Rik AI462003 Cabc1 mKIAA0451'}, {'ensembl_id': u'ENSG00000123815', 'db_id': 56, 'description': u'aarF domain containing kinase 4 <br />', 'symbol': u'ADCK4', 'species': u'Homo sapiens', 'aliases': u''}, {'ensembl_id': u'ENSMUSG00000003762', 'db_id': 46, 'description': u'aarF domain containing kinase 4 <br />', 'symbol': u'Adck4', 'species': u'Mus musculus', 'aliases': u'0610012P18Rik'}, {'ensembl_id': u'ENSG00000173137', 'db_id': 56, 'description': u'aarF domain containing kinase 5 <br />', 'symbol': u'ADCK5', 'species': u'Homo sapiens', 'aliases': u''}, {'ensembl_id': u'ENSG00000261694', 'db_id': 56, 'description': u'aarF domain containing kinase 5 <br />', 'symbol': u'ADCK5', 'species': u'Homo sapiens', 'aliases': u''}, {'ensembl_id': u'ENSMUSG00000022550', 'db_id': 46, 'description': u'aarF domain containing kinase 5 <br />', 'symbol': u'Adck5', 'species': u'Mus musculus', 'aliases': u''}]

def test_gene_autocomplete_period_all():
    explicit_search = None # not used
    max_number = 10000
    gene_search = 'AC008735.15'
    db_id = None
    returnData = Stemformatics_Gene.getAutoComplete(db,species_dict,gene_search,db_id,explicit_search,max_number)
    print returnData
    assert returnData == []


def test_gene_return_all_json():
    explicit_search = None # not used
    max_number = 10000
    data = Stemformatics_Gene.getAutoComplete(db,species_dict,'stat',None,explicit_search,max_number)
    returnData = json.dumps(data)
    print returnData
    assert returnData == '[{"ensembl_id": "ENSG00000115415", "db_id": 56, "description": "signal transducer and activator of transcription 1, 91kDa <br />", "symbol": "STAT1", "species": "Homo sapiens", "aliases": "CANDF7 ISGF-3 STAT91"}, {"ensembl_id": "ENSMUSG00000026104", "db_id": 46, "description": "signal transducer and activator of transcription 1 <br />", "symbol": "Stat1", "species": "Mus musculus", "aliases": "2010005J02Rik AA408197"}, {"ensembl_id": "ENSG00000170581", "db_id": 56, "description": "signal transducer and activator of transcription 2, 113kDa <br />", "symbol": "STAT2", "species": "Homo sapiens", "aliases": "ISGF-3 P113 STAT113"}, {"ensembl_id": "ENSMUSG00000040033", "db_id": 46, "description": "signal transducer and activator of transcription 2 <br />", "symbol": "Stat2", "species": "Mus musculus", "aliases": "1600010G07Rik AW496480"}, {"ensembl_id": "ENSG00000168610", "db_id": 56, "description": "signal transducer and activator of transcription 3 (acute-phase response factor) <br />", "symbol": "STAT3", "species": "Homo sapiens", "aliases": "APRF HIES"}, {"ensembl_id": "ENSMUSG00000004040", "db_id": 46, "description": "signal transducer and activator of transcription 3 <br />", "symbol": "Stat3", "species": "Mus musculus", "aliases": "1110034C02Rik AW109958 Aprf"}, {"ensembl_id": "ENSG00000138378", "db_id": 56, "description": "signal transducer and activator of transcription 4 <br />", "symbol": "STAT4", "species": "Homo sapiens", "aliases": "SLEB11"}, {"ensembl_id": "ENSMUSG00000062939", "db_id": 46, "description": "signal transducer and activator of transcription 4 <br />", "symbol": "Stat4", "species": "Mus musculus", "aliases": ""}, {"ensembl_id": "ENSG00000126561", "db_id": 56, "description": "signal transducer and activator of transcription 5A <br />", "symbol": "STAT5A", "species": "Homo sapiens", "aliases": "MGF STAT5"}, {"ensembl_id": "ENSMUSG00000004043", "db_id": 46, "description": "signal transducer and activator of transcription 5A <br />", "symbol": "Stat5a", "species": "Mus musculus", "aliases": "AA959963 STAT5"}, {"ensembl_id": "ENSG00000173757", "db_id": 56, "description": "signal transducer and activator of transcription 5B <br />", "symbol": "STAT5B", "species": "Homo sapiens", "aliases": "STAT5"}, {"ensembl_id": "ENSMUSG00000020919", "db_id": 46, "description": "signal transducer and activator of transcription 5B <br />", "symbol": "Stat5b", "species": "Mus musculus", "aliases": ""}, {"ensembl_id": "ENSG00000166888", "db_id": 56, "description": "signal transducer and activator of transcription 6, interleukin-4 induced <br />", "symbol": "STAT6", "species": "Homo sapiens", "aliases": "D12S1644 IL-4-STAT STAT6B STAT6C"}, {"ensembl_id": "ENSMUSG00000002147", "db_id": 46, "description": "signal transducer and activator of transcription 6 <br />", "symbol": "Stat6", "species": "Mus musculus", "aliases": ""}, {"ensembl_id": "ENSG00000126549", "db_id": 56, "description": "statherin <br />", "symbol": "STATH", "species": "Homo sapiens", "aliases": "STR"}, {"ensembl_id": "ENSG00000172889", "db_id": 56, "description": "EGF-like-domain, multiple 7 <br />", "symbol": "EGFL7", "species": "Homo sapiens", "aliases": "NEU1 RP11-251M1.2 VE-STATIN ZNEU1"}, {"ensembl_id": "ENSG00000134759", "db_id": 56, "description": "elongator acetyltransferase complex subunit 2 <br />", "symbol": "ELP2", "species": "Homo sapiens", "aliases": "SHINC-2 STATIP1 StIP"}, {"ensembl_id": "ENSMUSG00000024271", "db_id": 46, "description": "elongation protein 2 homolog (S. cerevisiae) <br />", "symbol": "Elp2", "species": "Mus musculus", "aliases": "AU023723 Epl2 StIP1 Statip1"}, {"ensembl_id": "ENSG00000123609", "db_id": 56, "description": "N-myc (and STAT) interactor <br />", "symbol": "NMI", "species": "Homo sapiens", "aliases": ""}, {"ensembl_id": "ENSMUSG00000026946", "db_id": 46, "description": "N-myc (and STAT) interactor <br />", "symbol": "Nmi", "species": "Mus musculus", "aliases": ""}]'
    
    


