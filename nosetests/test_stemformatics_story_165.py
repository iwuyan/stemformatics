"""

    nosetests --with-pylons=config.ini guide/model/stemformatics/test_stemformatics_dataset.py


"""

import logging
log = logging.getLogger(__name__)


import sqlalchemy as SA
from sqlalchemy import or_, and_, desc

import re
import string
import json

from guide.model.stemformatics import *
from guide.model.tfv import *

from guide.lib.state import *
from pylons import config
useSqlSoup = True


def test_check_dataset_availability():
    human_db = config['human_db']
    mouse_db = config['mouse_db']
        
    returnData = Stemformatics_Dataset.check_dataset_availability(db,44,1000)
    print returnData
    assert returnData == False
    
def test_check_dataset_availability_1():
    human_db = config['human_db']
    mouse_db = config['mouse_db']
        
    returnData = Stemformatics_Dataset.check_dataset_availability(db,18,1000)
    print returnData
    assert returnData == True
    

def test_getDatabaseID_1000_no_override():
    human_db = config['human_db']
    mouse_db = config['mouse_db']
        
    returnData = Stemformatics_Dataset.getDatabaseID(db,1000,human_db,mouse_db,44)
    print returnData
    assert returnData == None
    
def test_getDatabaseID_1000_override():    
    
    human_db = config['human_db']
    mouse_db = config['mouse_db']
    
    
    returnData = Stemformatics_Dataset.getDatabaseID(db,1000,human_db,mouse_db,18)
    print returnData
    assert returnData == 56
    


def test_getHandle_1000_override():    
    
    
    returnData = Stemformatics_Dataset.getHandle(db,1000,18)
    print returnData
    assert returnData == "Watkins_2009_19228925"
    
    
    returnData = Stemformatics_Dataset.getHandle(db,1000,44)
    print returnData
    assert returnData == None
    
        
def test_get_all_dataset_details():
    returnData = Stemformatics_Dataset.getAllDatasetDetails(db,18)
    
    # print returnData[5005]
    # print returnData[1000]
    assert 1000 in returnData
        
    returnData = Stemformatics_Dataset.getAllDatasetDetails(db,44)
    
    # print returnData[5005]
    # print returnData[1000]
    assert 1000 not in returnData


def test_getDataset_details():        
    returnData = Stemformatics_Dataset.getDatasetDetails(db,1000,44)
    
    assert returnData is None
    
    returnData = Stemformatics_Dataset.getDatasetDetails(db,1000,18)
    
    print returnData[1000]
    assert 1000 in returnData
    


def test_getDatasetMetadataDetails():
    returnData = Stemformatics_Dataset.getDatasetMetadataDetails(db,1000,44)
    assert returnData is None
    
    returnData = Stemformatics_Dataset.getDatasetMetadataDetails(db,1000,18)
    
    print returnData
    assert "detectionThreshold" in returnData
    

def test_getExpressionDatsetMetadata():
    returnData = Stemformatics_Dataset.getExpressionDatasetMetadata(db,1000,44)
    assert returnData is None
    returnData = Stemformatics_Dataset.getExpressionDatasetMetadata(db,1000,18)
    print returnData
    assert 'detection_threshold' in returnData
    
    
    

def test_getDatasetListForSpecies():
    returnData = Stemformatics_Dataset.getDatasetListForSpecies(db,1000,'Homo sapiens',44)
    print returnData
    assert returnData is None
    
    returnData = Stemformatics_Dataset.getDatasetListForSpecies(db,1000,'Homo sapiens',18)
    print returnData
    assert 'Hough_2009_19890402' in returnData
