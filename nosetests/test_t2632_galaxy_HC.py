import unittest
import logging
log = logging.getLogger(__name__)

from bioblend import galaxy
from bioblend.galaxy.tools.inputs import inputs, dataset

from pylons import request, response, session, tmpl_context as c, url, app_globals as g,config

from guide.model.stemformatics import *

def test_connect_to_galaxy():
    result = Stemformatics_Galaxy.connect_to_galaxy()
    assert result is not None

def test_run_hello_world():
    galaxyInstance = galaxy.GalaxyInstance(url='http://dev4.stemformatics.org:8080', key='f03bf2800c50683f79282bf180ac8d30')
    historyClient = galaxy.histories.HistoryClient(galaxyInstance)
    toolClient = galaxy.tools.ToolClient(galaxyInstance)

    history = historyClient.create_history('hello_world')

    random_lines_inputs = inputs().set("input", "1234567")
    result = toolClient.run_tool(history_id= history['id'], tool_id='hello_world', tool_inputs= random_lines_inputs)
    print(result)
    assert result is not None

def test_run_HC():
    from guide.model.stemformatics.stemformatics_galaxy import Stemformatics_Galaxy
    galaxyInstance = galaxy.GalaxyInstance(url='http://dev4.stemformatics.org:8080', key='f03bf2800c50683f79282bf180ac8d30')
    job_id = 0000
    uid = 629
    file_path = '/mnt/data/portal_data/pylons-data/prod/jobs/StemformaticsQueue/4770/job.gct'
    result = Stemformatics_Galaxy.run_HC_tool(galaxyInstance,job_id,file_path,uid)
    print(result)
    assert result is not None

def test_hc_inputs_tools():
    galaxyInstance = galaxy.GalaxyInstance(url='http://dev4.stemformatics.org:8080', key='f03bf2800c50683f79282bf180ac8d30')
    job_id = '4725'
    file_path = '/var/www/pylons-data/prod/jobs/StemformaticsQueue/4725/job.gct'
    ds_id = '5003'
    user_id = '629'
    result = Stemformatics_Galaxy.run_hc_input_tool(galaxyInstance,job_id,file_path,ds_id,user_id)
    assert result is not None

def test_return_job_status():
    from guide.model.stemformatics.stemformatics_galaxy import Stemformatics_Galaxy
    galaxyInstance = galaxy.GalaxyInstance(url='http://dev4.stemformatics.org:8080', key='f03bf2800c50683f79282bf180ac8d30')
    job_list = [4781]
    server_name= 'dev2-s4m'
    result = Stemformatics_Galaxy.return_job_status(galaxyInstance, job_list,server_name)
    print(result)
    assert result['4781']['state'] == 'ok'

def test_job_update_from_s4m_job():
    job =4702;
    job_id = 'f03bf2800c50683f79282bf180ac8d30'
    result = Stemformatics_Job.update_job_status(job,1,'Galaxy HC',job_id,db)
    print result
    assert result is not None

def test_delete_galaxy_dataset():
    from guide.model.stemformatics.stemformatics_galaxy import Stemformatics_Galaxy
    galaxyInstance = galaxy.GalaxyInstance(url='http://dev4.stemformatics.org:8080', key='f03bf2800c50683f79282bf180ac8d30')
    job_list = [4791]
    server_name = 'dev2'
    result = Stemformatics_Galaxy.delete_bulk_jobs(galaxyInstance, job_list, server_name)
    assert result is None
