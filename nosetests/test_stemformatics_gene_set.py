import logging
log = logging.getLogger(__name__)


import sqlalchemy as SA
from sqlalchemy import or_, and_, desc

import re
import string
import json

from guide.model.stemformatics import *
from guide.model.tfv import *

from guide.lib.state import *

useSqlSoup = True

log.debug('Testing in guide/model/stemformatics/test_stemformatics_gene_set.py')

human_db_id = 56
mouse_db_id = 46

gene_set_description_default = "THis is a test"

# tests addGeneSet, getGeneSets and getGeneSetData
def test_gene_set_add():
    
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    # print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    # print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
        
    uid = new_user.uid
    gene_set_name = "Booger"
    db_id = human_db_id
    list_of_genes = ['ENSG00000132613','ENSG00000143322','ENSG00000139645']
    
    
    # db,uid,gene_set_name,gene_set_description,db_id,list_of_genes
    returnData = Stemformatics_Gene_Set.addGeneSet(db,uid,gene_set_name,gene_set_description_default,db_id,list_of_genes)
    
    # print returnData
    # assert returnData == True

    checkData = Stemformatics_Gene_Set.getGeneSets(db,uid)
    
    # print checkData[0]
    
    assert checkData[0].gene_set_name.strip() == 'Booger'
    assert checkData[0].db_id == human_db_id
    
    
    getData = Stemformatics_Gene_Set.getGeneSetData(db,uid,checkData[0].id)
    
    print getData[1]
    assert getData[1][0].gene_id.strip() == 'ENSG00000132613'
    
    
# get getGeneSets
# already tested if right uid and gene sets (see first test)

# test if wrong uid and test if right uid but no gene sets
def test_getGeneSets_error_1():
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    # print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
        
    uid = new_user.uid
    gene_set_name = "Booger"
    db_id = human_db_id
    list_of_genes = ['ENSG00000132613','ENSG00000143322','ENSG00000139645']
    
    checkData = Stemformatics_Gene_Set.getGeneSets(db,uid)
    print checkData
    assert checkData == []
    
    returnData = Stemformatics_Gene_Set.addGeneSet(db,uid,gene_set_name,gene_set_description_default,db_id,list_of_genes)
    
    print returnData
    # assert returnData == True

    checkData = Stemformatics_Gene_Set.getGeneSets(db,8000000)
    print checkData
    assert checkData == []
    



    
# get getGeneSetData
# already tested if right uid and right gene_set_id (see first test)


# test if wrong uid, right gene_set_id
def test_getGeneSetData_error1():
    
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    # print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    # print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
        
    uid = new_user.uid
    gene_set_name = "Booger"
    db_id = human_db_id
    list_of_genes = ['ENSG00000132613','ENSG00000143322','ENSG00000139645']
    
    returnData = Stemformatics_Gene_Set.addGeneSet(db,uid,gene_set_name,gene_set_description_default,db_id,list_of_genes)
    
    # print returnData
    # assert returnData == True
    
    checkData = Stemformatics_Gene_Set.getGeneSets(db,uid)
    print checkData
        
    assert len(checkData) == 1
    
    gene_set_id = checkData[0].id
    
    assert gene_set_id == returnData
    
    result = Stemformatics_Gene_Set.getGeneSetData(db,uid + 1,gene_set_id)
    
    print result
    
    assert result == None

# test if right uid, but wrong gene_set_id
def test_getGeneSetData_error2():
    
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    # print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    # print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
        
    uid = new_user.uid
    gene_set_name = "Booger"
    db_id = human_db_id
    list_of_genes = ['ENSG00000132613','ENSG00000143322','ENSG00000139645']
    
    returnData = Stemformatics_Gene_Set.addGeneSet(db,uid,gene_set_name,gene_set_description_default,db_id,list_of_genes)
    
    # print returnData
    # assert returnData == True
    
    checkData = Stemformatics_Gene_Set.getGeneSets(db,uid)
    print checkData
        
    assert len(checkData) == 1
    
    gene_set_id = checkData[0].id
    
    assert returnData == gene_set_id
    
    result = Stemformatics_Gene_Set.getGeneSetData(db,uid,gene_set_id + 1)
    
    print result
    
    assert result == None    
    
    
    
# check add_gene_to_set


# test gene is ambiguous
def test_add_gene_to_set_error1():
    
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    # print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    # print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
        
    uid = new_user.uid
    gene_set_name = "Booger"
    db_id = human_db_id
    list_of_genes = ['ENSG00000132613','ENSG00000143322','ENSG00000139645']
    
    returnData = Stemformatics_Gene_Set.addGeneSet(db,uid,gene_set_name,gene_set_description_default,db_id,list_of_genes)
    
    # print returnData
    # assert returnData == True

    checkData = Stemformatics_Gene_Set.getGeneSets(db,uid)
    print checkData
        
    assert len(checkData) == 1
    
    gene_set_id = checkData[0].id
    
    assert returnData == gene_set_id

    gene = 'STAT'

    result = Stemformatics_Gene_Set.add_gene_to_set(db,uid,gene_set_id,db_id,gene)
    print result
    assert result == None
    
        
# test gene is not found        
def test_add_gene_to_set_error2():
    
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    # print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    # print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
        
    uid = new_user.uid
    gene_set_name = "Booger"
    db_id = human_db_id
    list_of_genes = ['ENSG00000132613','ENSG00000143322','ENSG00000139645']
    
    returnData = Stemformatics_Gene_Set.addGeneSet(db,uid,gene_set_name,gene_set_description_default,db_id,list_of_genes)
    
    # print returnData
    # assert returnData == True

    checkData = Stemformatics_Gene_Set.getGeneSets(db,uid)
    print checkData
        
    assert len(checkData) == 1
    
    gene_set_id = checkData[0].id
    
    assert returnData == gene_set_id

    gene = 'STATELYROCKING'

    result = Stemformatics_Gene_Set.add_gene_to_set(db,uid,gene_set_id,db_id,gene)
    print result
    assert result == None


# test gene is already in there    
def test_add_gene_to_set_error3():
    
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    # print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    # print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
        
    uid = new_user.uid
    gene_set_name = "Booger"
    db_id = human_db_id
    list_of_genes = ['ENSG00000132613','ENSG00000143322','ENSG00000139645']
    
    returnData = Stemformatics_Gene_Set.addGeneSet(db,uid,gene_set_name,gene_set_description_default,db_id,list_of_genes)
    
    # print returnData
    # assert returnData == True

    checkData = Stemformatics_Gene_Set.getGeneSets(db,uid)
    print checkData
        
    assert len(checkData) == 1
    
    gene_set_id = checkData[0].id
    
    assert returnData == gene_set_id

    gene = 'ENSG00000139645'

    result = Stemformatics_Gene_Set.add_gene_to_set(db,uid,gene_set_id,db_id,gene)
    print result
    assert result == None
    

# test uid and gene_set_id belong together
def test_add_gene_to_set_error4():
    
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    # print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    # print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
        
    uid = new_user.uid
    gene_set_name = "Booger"
    db_id = human_db_id
    list_of_genes = ['ENSG00000132613','ENSG00000143322','ENSG00000139645']
    
    returnData = Stemformatics_Gene_Set.addGeneSet(db,uid,gene_set_name,gene_set_description_default,db_id,list_of_genes)
    
    # print returnData
    # assert returnData == True

    checkData = Stemformatics_Gene_Set.getGeneSets(db,uid)
    print checkData
        
    assert len(checkData) == 1
    
    gene_set_id = checkData[0].id + 1

    assert returnData == gene_set_id - 1

    gene = 'PINK1'

    result = Stemformatics_Gene_Set.add_gene_to_set(db,uid,gene_set_id,db_id,gene)
    print result
    assert result == None


# test successful
def test_add_gene_to_set_success():
    
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    # print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    # print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
        
    uid = new_user.uid
    gene_set_name = "Booger"
    db_id = human_db_id
    list_of_genes = ['ENSG00000132613','ENSG00000143322','ENSG00000139645']
    
    returnData = Stemformatics_Gene_Set.addGeneSet(db,uid,gene_set_name,gene_set_description_default,db_id,list_of_genes)
    
    # print returnData
    # assert returnData == True

    checkData = Stemformatics_Gene_Set.getGeneSets(db,uid)
    # print checkData
        
    assert len(checkData) == 1
    
    gene_set_id = checkData[0].id
    
    assert returnData == gene_set_id
    
    gene = 'PINK1'

    result = Stemformatics_Gene_Set.add_gene_to_set(db,uid,gene_set_id,db_id,gene)
    # print result
    assert result == True
    
    getData = Stemformatics_Gene_Set.getGeneSetData(db,uid,gene_set_id)
    print getData[1][0]
    assert getData[1][0].gene_id.strip() == 'ENSG00000132613'
    


        
# check delete_gene_set




# test uid and gene_set_id belong together
def test_delete_gene_set_error():
    
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    # print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    # print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
        
    uid = new_user.uid
    gene_set_name = "Booger"
    db_id = human_db_id
    list_of_genes = ['ENSG00000132613','ENSG00000143322','ENSG00000139645']
    
    returnData = Stemformatics_Gene_Set.addGeneSet(db,uid,gene_set_name,gene_set_description_default,db_id,list_of_genes)
    
    print returnData
    # assert returnData == True

    checkData = Stemformatics_Gene_Set.getGeneSets(db,uid)
    print checkData
        
    assert len(checkData) == 1
    
    gene_set_id = checkData[0].id + 1 
    assert returnData == gene_set_id - 1
    
    result = Stemformatics_Gene_Set.delete_gene_set(db,uid,gene_set_id)
    
    assert result == None
    
# test successful
def test_delete_gene_set_success():
    
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    # print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    # print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
        
    uid = new_user.uid
    gene_set_name = "Booger"
    db_id = human_db_id
    list_of_genes = ['ENSG00000132613','ENSG00000143322','ENSG00000139645']
    
    returnData = Stemformatics_Gene_Set.addGeneSet(db,uid,gene_set_name,gene_set_description_default,db_id,list_of_genes)
    
    # print returnData
    #assert returnData == True

    checkData = Stemformatics_Gene_Set.getGeneSets(db,uid)
    print checkData
        
    assert len(checkData) == 1
    
    gene_set_id = checkData[0].id

    assert returnData == gene_set_id
    
    result = Stemformatics_Gene_Set.delete_gene_set(db,uid,gene_set_id)
    
    assert result == True
    
    checkData = Stemformatics_Gene_Set.getGeneSets(db,uid)
    print checkData
        
    assert len(checkData) == 0
    
# check delete_gene_set_item

# test successful
def test_delete_gene_set_item_success():
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    # print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    # print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
        
    uid = new_user.uid
    gene_set_name = "Booger"
    db_id = human_db_id
    list_of_genes = ['ENSG00000132613','ENSG00000143322','ENSG00000139645']
    
    returnData = Stemformatics_Gene_Set.addGeneSet(db,uid,gene_set_name,gene_set_description_default,db_id,list_of_genes)
    
    # print returnData
    # assert returnData == True

    checkData = Stemformatics_Gene_Set.getGeneSets(db,uid)
    print checkData
        
    assert len(checkData) == 1
    
    gene_set_id = checkData[0].id
    assert returnData == gene_set_id
    
    getData = Stemformatics_Gene_Set.getGeneSetData(db,uid,gene_set_id)
    
    gene_set_item_id = getData[1][0].id 
    assert getData[1][0].gene_id.strip() == 'ENSG00000132613'
    
    
    
    result = Stemformatics_Gene_Set.delete_gene_set_item(db,uid,gene_set_item_id)
    
    assert result == gene_set_id
    
    getData = Stemformatics_Gene_Set.getGeneSetData(db,uid,gene_set_id)
    print getData[1]
    assert getData[1][0].gene_id.strip() == 'ENSG00000139645'
    assert getData[1][1].gene_id.strip() == 'ENSG00000143322'
    
    
    
# test uid and gene_set_id belong together
def test_delete_gene_set_item_error1():
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    # print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    # print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
        
    uid = new_user.uid
    gene_set_name = "Booger"
    db_id = human_db_id
    list_of_genes = ['ENSG00000132613','ENSG00000143322','ENSG00000139645']
    
    returnData = Stemformatics_Gene_Set.addGeneSet(db,uid,gene_set_name,gene_set_description_default,db_id,list_of_genes)
    
    # print returnData
    # assert returnData == True

    checkData = Stemformatics_Gene_Set.getGeneSets(db,uid)
    print checkData
        
    assert len(checkData) == 1
    
    gene_set_id = checkData[0].id
    assert returnData == gene_set_id
    
    getData = Stemformatics_Gene_Set.getGeneSetData(db,uid,gene_set_id)
    
    gene_set_item_id = getData[1][0].id 
    assert getData[1][0].gene_id.strip() == 'ENSG00000132613'
    

    uid = uid + 1
    
    result = Stemformatics_Gene_Set.delete_gene_set_item(db,uid,gene_set_item_id)
    
    assert result == None

# test false gene_set_item_id
def test_delete_gene_set_item_error2():
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    # print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    # print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
        
    uid = new_user.uid
    gene_set_name = "Booger"
    db_id = human_db_id
    list_of_genes = ['ENSG00000132613','ENSG00000143322','ENSG00000139645']
    
    returnData = Stemformatics_Gene_Set.addGeneSet(db,uid,gene_set_name,gene_set_description_default,db_id,list_of_genes)
    
    # print returnData
    # assert returnData == True

    checkData = Stemformatics_Gene_Set.getGeneSets(db,uid)
    print checkData
        
    assert len(checkData) == 1
    
    gene_set_id = checkData[0].id
    assert returnData == gene_set_id
    
    getData = Stemformatics_Gene_Set.getGeneSetData(db,uid,gene_set_id)
    
    gene_set_item_id = getData[1][0].id 
    
    print getData[1][0]
    assert getData[1][0].gene_id == 'ENSG00000132613'
    
    gene_set_item_id = 21321646465464
    
    result = Stemformatics_Gene_Set.delete_gene_set_item(db,uid,gene_set_item_id)
    
    assert result == None



# check getGeneSet
# test uid and gene_set_id belong together
def test_getGeneSet_error1():
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    # print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    # print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
        
    uid = new_user.uid
    gene_set_name = "Booger"
    db_id = human_db_id
    list_of_genes = ['ENSG00000132613','ENSG00000143322','ENSG00000139645']
    
    returnData = Stemformatics_Gene_Set.addGeneSet(db,uid,gene_set_name,gene_set_description_default,db_id,list_of_genes)
    
    # print returnData
    # assert returnData == True

    checkData = Stemformatics_Gene_Set.getGeneSets(db,uid)
    print checkData
        
    assert len(checkData) == 1
    
    gene_set_id = checkData[0].id
    assert returnData == gene_set_id

    
    result = Stemformatics_Gene_Set.getGeneSet(db,uid + 1 ,gene_set_id)
    
    assert result == None
    
    
    
# test successful
def test_getGeneSet_success():
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    # print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    # print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
        
    uid = new_user.uid
    gene_set_name = "Booger"
    db_id = human_db_id
    list_of_genes = ['ENSG00000132613','ENSG00000143322','ENSG00000139645']
    
    returnData = Stemformatics_Gene_Set.addGeneSet(db,uid,gene_set_name,gene_set_description_default,db_id,list_of_genes)
    
    # print returnData
    #assert returnData == True
    
    checkData = Stemformatics_Gene_Set.getGeneSets(db,uid)
    print checkData
        
    assert len(checkData) == 1
    
    gene_set_id = checkData[0].id
    
    assert gene_set_id == returnData

    
    result = Stemformatics_Gene_Set.getGeneSet(db,uid,gene_set_id)
    
    assert result.gene_set_name.strip() == 'Booger'


# check update_gene_set_name
# test uid and gene_set_id belong together
def test_update_gene_set_name_error():
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    # print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    # print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
        
    uid = new_user.uid
    gene_set_name = "Booger"
    db_id = human_db_id
    list_of_genes = ['ENSG00000132613','ENSG00000143322','ENSG00000139645']
    
    returnData = Stemformatics_Gene_Set.addGeneSet(db,uid,gene_set_name,gene_set_description_default,db_id,list_of_genes)
    
    # print returnData
    # assert returnData == True
    
    checkData = Stemformatics_Gene_Set.getGeneSets(db,uid)
    print checkData
        
    assert len(checkData) == 1
    
    gene_set_id = checkData[0].id
    
    gene_set_name = "another booger"
    
    result = Stemformatics_Gene_Set.update_gene_set_name(db,uid + 1,gene_set_id,gene_set_name)
    
    assert result == None
    
    
# test successful
def test_update_gene_set_name_success():
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    # print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    # print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
        
    uid = new_user.uid
    gene_set_name = "Booger"
    db_id = human_db_id
    list_of_genes = ['ENSG00000132613','ENSG00000143322','ENSG00000139645']
    
    returnData = Stemformatics_Gene_Set.addGeneSet(db,uid,gene_set_name,gene_set_description_default,db_id,list_of_genes)
    
    print returnData
    # assert returnData == True
    
    checkData = Stemformatics_Gene_Set.getGeneSets(db,uid)
    print checkData
        
    assert len(checkData) == 1
    
    gene_set_id = checkData[0].id
    
    assert gene_set_id == returnData

    gene_set_name = "another booger"
    
    result = Stemformatics_Gene_Set.update_gene_set_name(db,uid,gene_set_id,gene_set_name)
    
    assert result == True
    
    checkData = Stemformatics_Gene_Set.getGeneSets(db,uid)
    print checkData
        
    assert len(checkData) == 1
    
    assert checkData[0].gene_set_name.strip() == gene_set_name


    

def test_get_data_for_gct_download_success_and_failure_1000():
    
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    # print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    # print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
        
    uid = new_user.uid
    gene_set_name = "Booger"
    db_id = human_db_id
    list_of_genes = ['ENSG00000132613','ENSG00000143322','ENSG00000139645']
    
    returnData = Stemformatics_Gene_Set.addGeneSet(db,uid,gene_set_name,gene_set_description_default,db_id,list_of_genes)
    
    # print returnData
    # assert returnData == True

    checkData = Stemformatics_Gene_Set.getGeneSets(db,uid)
    
    # print checkData[0]
    
    assert checkData[0].gene_set_name.strip() == 'Booger'
    assert checkData[0].db_id == human_db_id
    
    
    getData = Stemformatics_Gene_Set.getGeneSetData(db,uid,checkData[0].id)
    
    # print getData[1][0]
    assert getData[1][0].gene_id.strip() == 'ENSG00000132613'       
    
    gene_set_id = returnData
    
    datasetID = 1000
    comparison_type = 'Sample Type'
    
    result = Stemformatics_Gene_Set.get_data_for_gct_download(db,uid,gene_set_id,datasetID,comparison_type)
    
    # print result
    # print result[0]['ABL2 ILMN_1753972']['40-2167-Erythroblast']
    
    assert result[0]['ABL2 ILMN_1753972']['40-2167-Erythroblast'] == {'sample_size': 1, 'comparison_type': 'erythroblast (EB)', 'expression_value': 3.2152633923504799, 'standard_deviation': 0.0}


    # testing now for disease state
    datasetID = 2000
    comparison_type = 'Disease State'
    
    result = Stemformatics_Gene_Set.get_data_for_gct_download(db,uid,gene_set_id,datasetID,comparison_type)
    
    # print result
    # print result[0]['ABL2 ILMN_1753972']['40-2167-Erythroblast']
    print result[0]['ABL2 ILMN_1753972']['Olfactory neurosphere-derived cell donor 11']
    assert result[0]['ABL2 ILMN_1753972']['Olfactory neurosphere-derived cell donor 11'] == {'sample_size': 2, 'expression_value': 6.7037257266508297, 'comparison_type': 'control hONS', 'standard_deviation': 1.1337686052754301}


    



    # Testing for wrong dataset ID
    datasetID = 99

    result = Stemformatics_Gene_Set.get_data_for_gct_download(db,uid,gene_set_id,datasetID,comparison_type)
    
    print result
    
    assert result == [{}, [], [], []]

def test_get_db_id_success_and_failure():    
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    # print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    # print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
        
    uid = new_user.uid
    gene_set_name = "Booger"
    db_id = human_db_id
    list_of_genes = ['ENSG00000132613','ENSG00000143322','ENSG00000139645']
    
    returnData = Stemformatics_Gene_Set.addGeneSet(db,uid,gene_set_name,gene_set_description_default,db_id,list_of_genes)
    
    # print returnData
    # assert returnData == True

    checkData = Stemformatics_Gene_Set.getGeneSets(db,uid)
    
    # print checkData[0]
    
    assert checkData[0].gene_set_name.strip() == 'Booger'
    assert checkData[0].db_id == human_db_id
    
    
    getData = Stemformatics_Gene_Set.getGeneSetData(db,uid,checkData[0].id)
    
    print getData[1][0]
    assert getData[1][0].gene_id.strip() == 'ENSG00000132613'
    
    gene_set_id = returnData

    resultTest = Stemformatics_Gene_Set.get_db_id(db,uid,gene_set_id)

    print resultTest 
    assert resultTest == 56
    
    uid = 81
    gene_set_id = 4
    resultTest = Stemformatics_Gene_Set.get_db_id(db,uid,gene_set_id)
    
    print resultTest
    assert resultTest == None
    
    
def test_get_gene_set_name_success_and_failure():    
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    # print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    # print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
        
    uid = new_user.uid
    gene_set_name = "Booger"
    db_id = human_db_id
    list_of_genes = ['ENSG00000132613','ENSG00000143322','ENSG00000139645']
    
    returnData = Stemformatics_Gene_Set.addGeneSet(db,uid,gene_set_name,gene_set_description_default,db_id,list_of_genes)
    
    # print returnData
    # assert returnData == True

    checkData = Stemformatics_Gene_Set.getGeneSets(db,uid)
    
    # print checkData[0]
    
    assert checkData[0].gene_set_name.strip() == 'Booger'
    assert checkData[0].db_id == human_db_id
    
    
    getData = Stemformatics_Gene_Set.getGeneSetData(db,uid,checkData[0].id)
    
    print getData[1][0]
    assert getData[1][0].gene_id.strip() == 'ENSG00000132613'
    
    
    gene_set_id = returnData

    resultData = Stemformatics_Gene_Set.get_gene_set_name(db,uid,gene_set_id)
    print resultData
    assert resultData == 'Booger'
    
    gene_set_id = 1
    uid = 1
    
    resultData = Stemformatics_Gene_Set.get_gene_set_name(db,uid,gene_set_id)
    print resultData
    assert resultData == None


def test_get_species_success_and_failure():    
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    # print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    # print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
        
    uid = new_user.uid
    gene_set_name = "Booger"
    db_id = human_db_id
    list_of_genes = ['ENSG00000132613','ENSG00000143322','ENSG00000139645']
    
    returnData = Stemformatics_Gene_Set.addGeneSet(db,uid,gene_set_name,gene_set_description_default,db_id,list_of_genes)
    
    # print returnData
    # assert returnData == True

    checkData = Stemformatics_Gene_Set.getGeneSets(db,uid)
    
    # print checkData[0]
    
    assert checkData[0].gene_set_name.strip() == 'Booger'
    assert checkData[0].db_id == human_db_id
    
    
    getData = Stemformatics_Gene_Set.getGeneSetData(db,uid,checkData[0].id)
    
    print getData[1][0]
    assert getData[1][0].gene_id == 'ENSG00000132613'
    
    gene_set_id = returnData
    
    resultData = Stemformatics_Gene_Set.get_species(db,uid,gene_set_id)
    
    print resultData
    
    assert resultData == 'Homo sapiens'
    
    uid = 1
    gene_set_id = 1
    
    resultData = Stemformatics_Gene_Set.get_species(db,uid,gene_set_id)
    
    print resultData
    
    assert resultData == None
    
