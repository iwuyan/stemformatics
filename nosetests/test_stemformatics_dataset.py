"""

    nosetests --with-pylons=config.ini guide/model/stemformatics/test_stemformatics_dataset.py


"""

import logging
log = logging.getLogger(__name__)


import sqlalchemy as SA
from sqlalchemy import or_, and_, desc

import re
import string
import json

from guide.model.stemformatics import *
from guide.model.tfv import *

from guide.lib.state import *
from pylons import config
useSqlSoup = True

log.debug('Testing in guide/model/stemformatics/test_stemformatics_dataset.py')

'''def test_getHandle_1000():
    returnData = Stemformatics_Dataset.getHandle(db,1000)
    print returnData
    assert returnData == 'Blood cells (human)'

def test_getHandle_2000():
    returnData = Stemformatics_Dataset.getHandle(db,2000)
    print returnData
    assert returnData == 'hONS cells' '''
    
def test_getHandle_3000():
    returnData = Stemformatics_Dataset.getHandle(db,3000)
    print returnData
    assert returnData == 'human Breast (MaSC)'
    
def test_getHandle_4000():
    returnData = Stemformatics_Dataset.getHandle(db,4000)
    print returnData
    assert returnData == 'mouse Breast (MaSC)'

def test_getAllDatasetDetails():
    returnData = Stemformatics_Dataset.getAllDatasetDetails(db)
    print returnData
    assert returnData[1000]['pub_med_id'] == '19228925'
    assert returnData[2000]['pub_med_id'] == '19228925'
    assert returnData[3000]['pub_med_id'] == '19648928'
    assert returnData[4000]['pub_med_id'] == '20383121'
    
    assert returnData[1000]['handle'] == 'human Blood (mature cells)'
    ''' assert returnData[2000]['handle'] == 'hONS cells'
    assert returnData[3000]['handle'] == 'MaSCs (human)'
    assert returnData[4000]['handle'] == 'MaSCs (mouse)' '''
    
    
    assert returnData[1000]['probes'] == '46693'
    assert returnData[2000]['probes'] == '22184'
    assert returnData[3000]['probes'] == '48803'
    assert returnData[4000]['probes'] == '45281'


def test_getDatasetDetails_1000():
    returnData = Stemformatics_Dataset.getDatasetDetails(db,1000)
    print returnData
    assert returnData[1000]['pub_med_id'] == '19228925'
    assert returnData[1000]['handle'] == 'human Blood (mature cells)'
    assert returnData[1000]['probes'] == '46693'

def test_getDatasetDetails_4000():
    returnData = Stemformatics_Dataset.getDatasetDetails(db,4000)
    print returnData
    assert returnData[4000]['pub_med_id'] == '20383121'
    assert returnData[4000]['handle'] == 'mouse Breast (MaSC)'
    assert returnData[4000]['probes'] == '45281'
    
    
def test_getDatasetListForSpecies_human():
    returnData = Stemformatics_Dataset.getDatasetListForSpecies(db,1000,'Homo sapiens')
    print returnData
    assert returnData == {u'human Stem cells (ESC and iPS)': {'ds_id': 5002, 'handle': u'human Stem cells (ESC and iPS)'}, u'human Kidney cells': {'ds_id': 5004, 'handle': u'human Kidney cells'}, u'human Neuroepithelium (hONS)': {'ds_id': 2000, 'handle': u'human Neuroepithelium (hONS)'}, u'human Breast (MaSC)': {'ds_id': 3000, 'handle': u'human Breast (MaSC)'}, u'human Blood (mature cells)': {'ds_id': 1000, 'handle': u'human Blood (mature cells)'}}



def test_getDatasetListForSpecies_mouse():
    returnData = Stemformatics_Dataset.getDatasetListForSpecies(db,4000,'Mus musculus')
    print returnData
    assert returnData == {u'mouse Heart (iCMs)': {'ds_id': 5001, 'handle': u'mouse Heart (iCMs)'}, u'mouse Breast (MaSC)': {'ds_id': 4000, 'handle': u'mouse Breast (MaSC)'}}





def test_getDatasetMetadataDetails_1000():
    returnData = Stemformatics_Dataset.getDatasetMetadataDetails(db,1000)
    print returnData
    assert returnData == {u'detectionThreshold': u'5.00', u'sampleTypeDisplayGroups': u'{"erythroblast (EB)":0,"megakaryocyte (MK)":0,"granulocyte":1,"monocyte":1,"B lymphocyte":2,"cytotoxic T lymphocyte (Tc)":2,"helper T lymphocyte (Th)":2,"natural killer cell (NK)":3}', u'Title': u'Blood cells (human)', u'Experimental Design': u'Transcription profiling by array', u'Platform': u'Illumina HumanWG-6 V2', u'Contributor Name': u'Nick A Watkins', u'Description': u'Hematopoiesis is a carefully controlled process that is regulated by complex networks of transcription factors that are, in part, controlled by signals resulting from ligand binding to cell-surface receptors. To further understand hematopoiesis, we have compared gene expression profiles of human erythroblasts, megakaryocytes, B cells, cytotoxic and helper T cells, natural killer cells, granulocytes, and monocytes using whole genome microarrays. A bioinformatics analysis of these data was performed focusing on transcription factors, immunoglobulin superfamily members, and lineage-specific transcripts. We observed that the numbers of lineage-specific genes varies by 2 orders of magnitude, ranging from 5 for cytotoxic T cells to 878 for granulocytes. In addition, we have identified novel coexpression patterns for key transcription factors involved in hematopoiesis (eg, GATA3-GFI1 and GATA2-KLF1). This study represents the most comprehensive analysis of gene expression in hematopoietic cells to date and has identified genes that play key roles in lineage commitment and cell function. The data, which are freely accessible, will be invaluable for future studies on hematopoiesis and the role of specific genes and will also aid the understanding of the recent genome-wide association studies.', u'PubMed ID': u'19228925', u'Authors': u'Watkins, Nicholas A.; Gusnanto, Arief; de Bono, Bernard; De, Subhajyoti; Miranda-Saavedra, Diego; Hardie, Debbie L.; Angenent, Will G. J.; Attwood, Antony P.; Ellis, Peter D.; Erber, Wendy; Foad, Nicola S.; Garner, Stephen F.; Isacke, Clare M.; Jolley, Jennifer; Koch, Kerstin; Macaulay, Iain C.; Morley, Sarah L.; Rendon, Augusto; Rice, Kate M.; Taylor, Niall; Thijssen-Timmer, Daphne C.; Tijssen, Marloes R.; van der Schoot, C. Ellen; Wernisch, Lorenz; Winzer, Thilo; Dudbridge, Frank; Buckley, Christopher D.; Langford, Cordelia F.; Teichmann, Sarah; Gottgens, Berthold; Ouwehand, Willem H.; on behalf of the Bloodomics Consortium', u'Assay Version': u'V2', u'Release Date': u'2009-02-06', u'probesDetected': u'13608', u'Contributor Email': u'naw23@cam.ac.uk', u'Assay Platform': u'HumanWG-6', u'maxReplicates': u'1', u'Contact Name': u'Nick A Watkins', u'topDifferentiallyExpressedGenes': {u'FAM171A1': {'db_id': 56, 'ensemblID': u'ENSG00000148468'}, u'MAD2L1': {'db_id': 56, 'ensemblID': u'ENSG00000164109'}, u'CDCA7': {'db_id': 56, 'ensemblID': u'ENSG00000144354'}, u'MLLT11': {'db_id': 56, 'ensemblID': u'ENSG00000213190'}, u'SALL2': {'db_id': 56, 'ensemblID': u'ENSG00000165821'}}, u'Organism': u'Homo sapiens', u'minReplicates': u'1', 'breakDown': {u'Sample Type: erythroblast (EB)': 4, u'Gender: male': 24, u'Gender: female': 18, u'Sample Type: helper T lymphocyte (Th)': 7, u'Sample Type: granulocyte': 7, u'Organism Part: bone marrow': 8, u'Labelling: biotin': 50, u'Disease State: normal': 50, u'Sample Type: cytotoxic T lymphocyte (Tc)': 7, u'Gender: unknown': 8, u'Sample Type: B lymphocyte': 7, u'Organism: Homo sapiens': 50, u'Sample Type: natural killer cell (NK)': 7, u'Organism Part: blood': 42, u'Sample Type: megakaryocyte (MK)': 4, u'Sample Type: monocyte': 7}, u'probeCount': u'46693', u'sampleTypeDisplayOrder': u'megakaryocyte (MK),erythroblast (EB),granulocyte,monocyte,B lymphocyte,helper T lymphocyte (Th),cytotoxic T lymphocyte (Tc),natural killer cell (NK)', u'Publication Title': u'A HaemAtlas: characterizing gene expression in differentiated human blood cells', u'medianDatasetExpression': u'8.79', u'Accession': u'E-TABM-633', u'Publication Citation': u'Blood. 2009 May 7;113(19):4479-80.', u'Affiliation': u'University of Cambridge', u'Assay Manufacturer': u'Illumina', u'Contact Email': u'naw23@cam.ac.uk', u'limitSortBy': u'Sample Type'}




def test_getDatasetMetadataDetails_4000():
    returnData = Stemformatics_Dataset.getDatasetMetadataDetails(db,4000)
    print returnData
    assert returnData == {u'detectionThreshold': u'5.00', u'sampleTypeDisplayGroups': u'{"MaSC enriched (CD29 hi CD24+)":0,"luminal (CD29 lo CD24+)":1}', u'Title': u'Mouse mammary stem cells (MaSCs)', u'Experimental Design': u'Transcription profiling by array', u'Platform': u'Illumina MouseWG-6 v2.0', u'Geo Accession': u'GSE20402', u'Contributor Name': u'Gordon K Smyth', u'Description': u'The ovarian hormones oestrogen and progesterone profoundly influence breast cancer risk, underpinning the benefit of endocrine therapies in the treatment of breast cancer. Modulation of their effects through ovarian ablation or chemoprevention strategies also significantly decreases breast cancer incidence. Conversely, there is an increased risk of breast cancer associated with pregnancy in the short term. The cellular mechanisms underlying these observations, however, are poorly defined. Here we demonstrate that mouse mammary stem cells (MaSCs) are highly responsive to steroid hormone signalling, despite lacking the oestrogen and progesterone receptors. Ovariectomy markedly diminished MaSC number and outgrowth potential in vivo, whereas MaSC activity increased in mice treated with oestrogen plus progesterone. Notably, even three weeks of treatment with the aromatase inhibitor letrozole was sufficient to reduce the MaSC pool. In contrast, pregnancy led to a transient 11-fold increase in MaSC numbers, probably mediated through paracrine signalling from RANK ligand. The augmented MaSC pool indicates a cellular basis for the short-term increase in breast cancer incidence that accompanies pregnancy. These findings further indicate that breast cancer chemoprevention may be achieved, in part, through suppression of MaSC function.', u'PubMed ID': u'20383121', u'Authors': u'Marie-Liesse Asselin-Labat, Francois Vaillant, Julie M. Sheridan, Bhupinder Pal, Di Wu, Evan R. Simpson, Hisataka Yasuda, Gordon K. Smyth, T. John Martin, Geoffrey J. Lindeman & Jane E. Visvader', u'Assay Version': u'V2', u'Release Date': u'2010-04-10', u'probesDetected': u'20395', u'Contributor Email': u'smyth@wehi.edu.au', u'Assay Platform': u'MouseWG-6', u'maxReplicates': u'1', u'Contact Name': u'Geoffrey J. Lindeman, Jane E. Visvader', u'topDifferentiallyExpressedGenes': {u'Slpi': {'db_id': 46, 'ensemblID': u'ENSMUSG00000017002'}, u'Lgr5': {'db_id': 46, 'ensemblID': u'ENSMUSG00000020140'}, u'Glycam1': {'db_id': 46, 'ensemblID': u'ENSMUSG00000022491'}, u'Fabp3': {'db_id': 46, 'ensemblID': u'ENSMUSG00000028773'}, u'Wap': {'db_id': 46, 'ensemblID': u'ENSMUSG00000000381'}}, u'Organism': u'Mus musculus', u'minReplicates': u'1', 'breakDown': {u'Sample Type: luminal (CD29 lo CD24+)': 7, u'Gender: female': 14, u'Organism Part: Mammary epithelium': 14, u'Sample Type: MaSC enriched (CD29 hi CD24+)': 7, u'Disease State: normal': 14, u'Organism: Mus musculus': 14, u'Labelling: Cy3': 14}, u'probeCount': u'45281', u'sampleTypeDisplayOrder': u'MaSC enriched (CD29 hi CD24+),luminal (CD29 lo CD24+)', u'Publication Title': u'Mouse mammary epithelial cell subpopulations from pregnant and virgin mice', u'medianDatasetExpression': u'8.89', u'Accession': u'E-GEOD-20402', u'Publication Citation': u'Nature 2010 June 10;465(7299):798-802', u'Affiliation': u'Walter and Eliza Hall Institute of Medical Research', u'Assay Manufacturer': u'Illumina', u'Contact Email': u'lindeman@wehi.edu.au, visvader@wehi.edu.au', u'limitSortBy': u'Sample Type'}



def test_getExpressionDatasetMetadata_1000():
    returnData = Stemformatics_Dataset.getExpressionDatasetMetadata(db,1000)
    print returnData
    assert returnData == {'sampleTypeDisplayOrder': u'megakaryocyte (MK),erythroblast (EB),granulocyte,monocyte,B lymphocyte,helper T lymphocyte (Th),cytotoxic T lymphocyte (Tc),natural killer cell (NK)', 'detection_threshold': u'5.00', 'sampleTypeDisplayGroups': u'{"erythroblast (EB)":0,"megakaryocyte (MK)":0,"granulocyte":1,"monocyte":1,"B lymphocyte":2,"cytotoxic T lymphocyte (Tc)":2,"helper T lymphocyte (Th)":2,"natural killer cell (NK)":3}', 'limit_sort_by': u'Sample Type', 'median_dataset_expression': u'8.79', 'species': u'Homo sapiens'}


def test_getExpressionDatasetMetadata_2000():
    returnData = Stemformatics_Dataset.getExpressionDatasetMetadata(db,2000)
    print returnData
    assert returnData == {'sampleTypeDisplayOrder': u'hONS,fibroblast', 'detection_threshold': u'5.00', 'sampleTypeDisplayGroups': u'{"hONS":0,"fibroblast":1}', 'limit_sort_by': u'Sample Type,Disease State', 'median_dataset_expression': u'8.93', 'species': u'Homo sapiens'}

def test_getExpressionDatasetMetadata_4000():
    returnData = Stemformatics_Dataset.getExpressionDatasetMetadata(db,4000)
    print returnData
    assert returnData == {'sampleTypeDisplayOrder': u'MaSC enriched (CD29 hi CD24+),luminal (CD29 lo CD24+)', 'detection_threshold': u'5.00', 'sampleTypeDisplayGroups': u'{"MaSC enriched (CD29 hi CD24+)":0,"luminal (CD29 lo CD24+)":1}', 'limit_sort_by': u'Sample Type', 'median_dataset_expression': u'8.89', 'species': u'Mus musculus'}


def test_getChipType_1000():
    returnData = Stemformatics_Dataset.getChipType(db,1000)
    print returnData
    assert returnData ==7

def test_getChipType_4000():
    returnData = Stemformatics_Dataset.getChipType(db,4000)
    print returnData
    assert returnData == 1


def test_getDatabaseID_1000():
    human_db = config['human_db']
    mouse_db = config['mouse_db']
        
    returnData = Stemformatics_Dataset.getDatabaseID(db,1000,human_db,mouse_db)
    print returnData
    assert returnData == 56
    
def test_getDatabaseID_4000():
    human_db = config['human_db']
    mouse_db = config['mouse_db']
    returnData = Stemformatics_Dataset.getDatabaseID(db,4000,human_db,mouse_db)
    print returnData
    assert returnData == 46
