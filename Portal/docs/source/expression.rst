.. expression classes

.. automodule:: expression

.. autoclass:: Expression
    :members:
    :inherited-members:
    :undoc-members:

.. autoclass:: ExpressionFactory
    :members:
    :inherited-members:
    :undoc-members:

.. autoclass:: ExpressionEncoder
   :members: