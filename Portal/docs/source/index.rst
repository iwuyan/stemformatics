.. GuIDE documentation master file, created by
   sphinx-quickstart on Fri Jul 30 18:11:20 2010.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

GuIDE: Genomic Information Database Explorer
============================================

Contents:

.. toctree::
   :maxdepth: 2

   models

   utils

   configuration

   building-env

   authors

Indices and tables
==================

 * :ref:`genindex`
 * :ref:`modindex`
 * :ref:`search`

