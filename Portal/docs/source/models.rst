.. models section

=================
GuIDE Data Models
=================

MAGMA: Malleable Amalgam of Genome Maps and  Annotations
--------------------------------------------------------
.. toctree::
   :maxdepth: 2

   magma

TFV: Transcriptome Framework Viewer
-----------------------------------
.. toctree::
   :maxdepth: 2

   tfv

ABACUS: Activity-Based Access Control and Usage Supervisor
----------------------------------------------------------

.. toctree::
   :maxdepth: 2

   abacus