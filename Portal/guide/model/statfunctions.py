import math

def mean(list_):
	'''Returns the mean of a list of values, even if list_ contains None's.'''
	x = [float(val) for val in list_ if val] # ignore missing values
	return sum(x)/len(x) if x else None

def sd(list_, mean):
	'''Returns the variance of a list of values, even if list_ contains None's.'''
	x = [float(val) for val in list_ if val]	# ignore missing values
	sum_XX=0
	variance=[]
	i=0
	for i in range(len(x)):
		X = x[i] - mean
		XX = pow(X, 2)
		sum_XX = sum_XX + XX
	var = sum_XX / len(x) 
	sd = pow(var,.5)
	return sd

def zScore(vals):
	'''Given a list of numbers, return a list of z-scores. 
	If there are None's in vals, these are returned as None's so len(vals)==len(zScore(vals)) always.
	'''
	m = mean(vals)
	s = sd(vals,m)
	return [(float(value)-m)/s if value else None for value in vals]
	
def foldChange(vals):
	'''Given a list of numbers, return a list of z-scores. 
	If there are None's in x, these are returned as None's so len(x)==len(zScore(x)).
	'''
	logs=[math.log(float(x),2) if x else None for x in vals]
	m = mean(logs)
	return [x-m if x else None for x in logs]

def calcPPMC(a, b):
    """\
An implementation of the Pearson Product Moment Correlation
calculation done on two python lists.   Assumes that a and b
are non-empty lists of numerics and are of equal length.

This is modeled on the formula described in Wikipedia here:
http://changingminds.org/explanations/research/analysis/pearson.htm

    r = SUM((xi - xbar)(y - ybar)) / ((n - 1) * sx * sy)

Where x and y are the variables, xi is a single value of x, 
xbar is the mean of all x's, n is the number of variables, and 
sx is the standard deviation of all x's.

To test:  run "python -m doctest ppmc.py":

>>> p = [1, 3, 5, 6, 8, 9, 6, 4, 3, 2]
>>> q = [2, 5, 6, 6, 7, 7, 5, 3, 1, 1]

>>> print round(calcPPMC(p, q), 4)
0.8547

(Note: this uses the `sample standard deviation` rather than the
standard deviation of the sample.)

"""
    from math import sqrt
    
    # By making N a float, all the other numbers and calculations
    # that use it will be coerced to float.  This obviates the
    # need to iterate through the two incoming lists to ensure
    # their elements are all float.  They need only be numeric.
    N = float(len(a))
    
    # Sanity check: must be non-empty lists
    #  and the length of each must be the same.
    if N < 1 or N != len(b):  return None
    
    # Calc the means
    a_bar = sum(a) / N
    b_bar = sum(b) / N    
    
    # Calc the diffs from the mean (not the variances)
    a_diff = map(lambda x: x - a_bar, a)
    b_diff = map(lambda x: x - b_bar, b)
    
    # Cal the sample std devs
    a_sdev = sqrt(sum(map(lambda x: x**2, a_diff)) / (N-1))
    b_sdev = sqrt(sum(map(lambda x: x**2, b_diff)) / (N-1))
    
    # Return the result.
    return sum(map(lambda x,y: (x * y), a_diff, b_diff)) / ((N - 1) * a_sdev * b_sdev)

def calcCorrelation(a,b):
	'''Takes two lists a,b of same length, and return the correlation (float)
	'''
	if len(a) != len(b): return None
	refFloat = [float(x) for x in a]
	refMean = sum(refFloat) / len(refFloat)
	mean = sum(b) / len(b)
	sum_XX = 0
	sum_YY = 0
	sum_XY = 0
	x = []
	for e in range(len(a)):
		X = refFloat[e] - refMean
		x.append(X)
		XX = pow(X,2)
		sum_XX = sum_XX + XX
	for k in range(len(b)):
		Y = b[k]- mean
		YY = pow(Y,2)
		sum_YY = sum_YY + YY
		XY = x[k]*Y
		sum_XY = sum_XY + XY
	r = sum_XY/pow((sum_XX*sum_YY),.5)
	return r

def multiCorr(list_):
	'''Not working yet for its intended purpose - ie. for more than 2 elements in list_. Simple generalisation
	of the formula for 2 elements does not seem to give a correct answer (correct answer given for 
	the 2 elements case). Presumably due to generalisation being incorrect.
	Return a Pearson Correlation coefficient given a list of lists. The following formula is used
	(see for example: http://www.vias.org/tmdatanaleng/cc_corr_coeff.html):
	let x = list_, n = length of each element of x, N = length of x, and all sums are over i from 1 to n,
	and all prods are over j from 1 to N:
	( sum(prod(x[j][i])) - prod(sum(x[j][i]))/n ) / prod(sqrt(sum(x[j][i]^2) - (sum(x[j][i]))^2/n)
	'''
	x = list_
	n = len(x[0])
	N = len(x)
	
	# calculate sum(prod(x[j][i]))
	sumprod = 0
	for i in range(n):
		prod = 1
		for j in range(N):
			prod = prod * x[j][i]
		sumprod = sumprod + prod
		
	# calculate prod(sum(x[j][i]))
	prodsum = 1
	for j in range(N):
		prodsum = prodsum * sum(x[j])
	
	# calculate prod(sqrt(sum(x[j][i]^2) - (sum(x[j][i]))^2/n)
	prodsqrt = 1
	for j in range(N):
		sumsq = 0
		for i in range(n):
			sumsq = sumsq + x[j][i]*x[j][i]
		prodsqrt = prodsqrt * pow(sumsq - pow(sum(x[j]),2)/n, .5)
		
	return (sumprod - prodsum/n)/prodsqrt
	
def calcVariance(x):
	'''Takes a list of floats or integers and returns the variance of the list.
	'''
	mean = sum(x) / len(x)
	sum_XX=0
	variance=[]
	i=0
	for i in range(len(x)):
		X = x[i] - mean
		XX = pow(X, 2)
		sum_XX = sum_XX + XX
	var = sum_XX / len(x)
	variance.append(var)
	return variance
	
	
