'''Even though the Base instance here can be used by classes in the other modules in this package in the usual way,
note that lazy loading of certain attributes may not work if it is not bound to a session. Hence this Base
attribute is redefined in model.init_model method using the session argument after a session has been created there.
'''
from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()
