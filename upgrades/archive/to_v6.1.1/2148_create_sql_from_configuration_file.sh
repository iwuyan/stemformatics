#!/bin/bash
temp_file=/tmp/2148.txt
temp_file_2=/tmp/2148-2.txt
original_file=/home/rowlandm/s4m/Portal/conf/qfab-production-pglocal.ini
final_file=/home/rowlandm/s4m/upgrades/to_v6.1.1/2148_add_config_values.sql

sed -n '/server:main/q;p' "$original_file" > "$temp_file"
sed -i '1,/\[DEFAULT\]/ d' "$temp_file"

sed -i '/^$/d' "$temp_file"
sed -i '/^#/ d' "$temp_file"
sed -i 's/\ =\ /=/g' "$temp_file"
sed -i 's/\"//g' "$temp_file"

cat "$temp_file" | awk -F "=" '{ print "INSERT INTO stemformatics.configs (ref_type,ref_id) values(\"" $1 "\",\"" $2 "\");"}'  > "$temp_file_2"

sed "s/\"/'/g" "$temp_file_2" > "$final_file"

vim "$final_file" 


# finally you can run 
# psql -U portaladmin -h localhost  portal_prod -f s4m/upgrades/to_v6.1.1/2148_add_configs.sql 
# psql -U portaladmin -h localhost  portal_prod -f s4m/upgrades/to_v6.1.1/2148_add_config_values.sql 
