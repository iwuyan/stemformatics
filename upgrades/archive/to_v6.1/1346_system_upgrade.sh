#!/bin/bash

# This is for T#1346 to upgrade the Centos to have prince and pdftoppm
# This allows stemformatics_export.py to run these two commands to 
# convert svg to pdf or png

run_or_skip () {
    echo -n " (r)un or (s)kip $1  "
    read run_or_skip
    echo $run_or_skip
    if [ "$run_or_skip" == "r" -o -z "$run_or_skip" ]; then
        return 0
    fi
    return 1
}

# Download prince and install
file=/tmp/prince.rpm

# install xpdf to install pdftoppm
run_or_skip "Install yum components like xpdf and rpm-build etc"
if [ $? == 0 ]; then 
	sudo yum install xpdf rpm-build cabextract ttmkfdir libtiff
fi 

run_or_skip "Download prince"
if [ $? == 0 ]; then 
	wget "http://www.princexml.com/download/prince-9.0-5.centos60.x86_64.rpm" -O $file
fi

run_or_skip "Install prince"
if [ $? == 0 ]; then 
	sudo rpm -Uvh  $file
fi

# Have to install msttcorefonts
run_or_skip "Download msttcorefonts 2.5.1"
if [ $? == 0 ]; then 
	wget "http://corefonts.sourceforge.net/msttcorefonts-2.5-1.spec"
fi 

run_or_skip "Build RPM for msttcorefonts"
if [ $? == 0 ]; then 
	rpmbuild -bb msttcorefonts-2.5-1.spec
fi 

run_or_skip "Install RPM for msttcorefonts"
if [ $? == 0 ]; then 
	sudo rpm -ivh $HOME/rpmbuild/RPMS/noarch/msttcorefonts-2.5-1.noarch.rpm
fi 

# test using current svg file
run_or_skip "Test converstion"
if [ $? == 0 ]; then 
	prince -v rohart_msc_test_6037_1426485054.2.svg -o test_prince.pdf
	pdftoppm test_prince.pdf -png -r 400 > test_pdftoppm.png
fi
