-- Adding biosamples_metadata for graph reannotation.

INSERT INTO biosamples_metadata (chip_type, chip_id,md_name,md_value,ds_id) select chip_type,chip_id,'Sample name','',ds_id from biosamples_metadata where md_name = 'Replicate Group ID' and ds_id in (select distinct ds_id from biosamples_metadata);

INSERT INTO biosamples_metadata (chip_type, chip_id,md_name,md_value,ds_id) select chip_type,chip_id,'Sample name long','',ds_id from biosamples_metadata where md_name = 'Replicate Group ID' and ds_id in (select distinct ds_id from biosamples_metadata);

INSERT INTO biosamples_metadata (chip_type, chip_id,md_name,md_value,ds_id) select chip_type,chip_id,'Sample type long',md_value,ds_id from biosamples_metadata where md_name = 'Sample Type' and ds_id in (select distinct ds_id from biosamples_metadata);

INSERT INTO biosamples_metadata (chip_type, chip_id,md_name,md_value,ds_id) select chip_type,chip_id,'Generic sample type',md_value,ds_id from biosamples_metadata where md_name = 'Sample Type' and ds_id in (select distinct ds_id from biosamples_metadata);

INSERT INTO biosamples_metadata (chip_type, chip_id,md_name,md_value,ds_id) select chip_type,chip_id,'Generic sample type long', md_value, ds_id from biosamples_metadata where md_name = 'Sample Type' and ds_id in (select distinct ds_id from biosamples_metadata);

update biosamples_metadata set md_name = 'Parental cell type' where (md_name = 'Derived From' or md_name = 'Derived from') and ds_id in (select distinct ds_id from biosamples_metadata);
INSERT INTO biosamples_metadata (chip_type, chip_id,md_name,md_value,ds_id) select chip_type,chip_id,'Parental cell type','',ds_id from biosamples_metadata where md_name = 'Replicate Group ID' and ds_id in (select distinct ds_id from biosamples_metadata) and ds_id not in (select distinct ds_id from biosamples_metadata where md_name = 'Parental cell type');

update biosamples_metadata set md_name = 'Final cell type' where (md_name = 'Differntiated To' or md_name = 'Differentiated to') and ds_id in (select distinct ds_id from biosamples_metadata);
INSERT INTO biosamples_metadata (chip_type, chip_id,md_name,md_value,ds_id) select chip_type,chip_id,'Final cell type','',ds_id from biosamples_metadata where md_name = 'Replicate Group ID' and ds_id in (select distinct ds_id from biosamples_metadata) and ds_id not in (select distinct ds_id from biosamples_metadata where md_name = 'Final cell type');

update biosamples_metadata set md_name = 'Reprogramming method' where md_name= 'Reprogramming Method';
INSERT INTO biosamples_metadata (chip_type, chip_id,md_name,md_value,ds_id) select chip_type,chip_id,'Reprogramming method','',ds_id from biosamples_metadata where md_name = 'Replicate Group ID' and ds_id in (select distinct ds_id from biosamples_metadata) and ds_id not in (select distinct ds_id from biosamples_metadata where md_name = 'Reprogramming method');

INSERT INTO biosamples_metadata (chip_type, chip_id,md_name,md_value,ds_id) select chip_type,chip_id,'Media','',ds_id from biosamples_metadata where md_name = 'Replicate Group ID' and ds_id in (select distinct ds_id from biosamples_metadata);

INSERT INTO biosamples_metadata (chip_type, chip_id,md_name,md_value,ds_id) select chip_type,chip_id,'Genetic modification','',ds_id from biosamples_metadata where md_name = 'Replicate Group ID' and ds_id in (select distinct ds_id from biosamples_metadata);

update biosamples_metadata set md_name = 'FACS profile' where md_name = 'Cells Sorted For' and ds_id in (select distinct ds_id from biosamples_metadata);
update biosamples_metadata set md_name = 'FACS profile' where md_name = 'Cell sorted for' and ds_id in (select distinct ds_id from biosamples_metadata);
INSERT INTO biosamples_metadata (chip_type, chip_id,md_name,md_value,ds_id) select chip_type,chip_id,'FACS profile','',ds_id from biosamples_metadata where md_name = 'Replicate Group ID' and ds_id in (select distinct ds_id from biosamples_metadata) and ds_id not in (select distinct ds_id from biosamples_metadata where md_name = 'FACS profile');

update biosamples_metadata set md_name = 'Sex' where ds_id in (select distinct ds_id from biosamples_metadata) and md_name = 'Gender';

update biosamples_metadata set md_name = 'Tissue/organism part' where ds_id in (select distinct ds_id from biosamples_metadata) and md_name = 'Tissue';

update biosamples_metadata set md_name = 'Developmental stage' where md_name = 'Developmental Stage' and ds_id in (select distinct ds_id from biosamples_metadata) and ds_id not in (select distinct ds_id from biosamples_metadata where md_name = 'Developmental stage');

INSERT INTO biosamples_metadata (chip_type, chip_id,md_name,md_value,ds_id) select chip_type,chip_id,'Developmental stage','',ds_id from biosamples_metadata where md_name = 'Replicate Group ID' and ds_id in (select distinct ds_id from biosamples_metadata) and ds_id not in (select distinct ds_id from biosamples_metadata where md_name = 'Developmental stage');

update biosamples_metadata set md_name = 'Cell line' where md_name = 'Cell Line' and ds_id in (select distinct ds_id from biosamples_metadata) and ds_id not in (select distinct ds_id from biosamples_metadata where md_name = 'Cell line');

INSERT INTO biosamples_metadata (chip_type, chip_id,md_name,md_value,ds_id) select chip_type,chip_id,'Cell line','',ds_id from biosamples_metadata where md_name = 'Replicate Group ID' and ds_id in (select distinct ds_id from biosamples_metadata) and ds_id not in (select distinct ds_id from biosamples_metadata where md_name = 'Cell line');

DELETE FROM stemformatics.stats_datasetsummary; INSERT INTO stemformatics.stats_datasetsummary SELECT bsm.ds_id, bsm.md_name, bsm.md_value, count(distinct bsm.chip_id) FROM biosamples_metadata bsm WHERE bsm.md_name IN ('Age', 'Cell line', 'Cell Type', 'FACS profile', 'Parental cell type', 'Developmental stage', 'Final cell type', 'Disease State', 'Sex', 'Organism', 'Organism Part', 'Pregnancy Status', 'Sample type long', 'Tissue/organism part') AND bsm.chip_id IN (SELECT chip_id FROM biosamples_metadata WHERE md_name='Replicate' AND md_value='1') GROUP BY bsm.ds_id, bsm.md_name, bsm.md_value ORDER BY bsm.ds_id, bsm.md_name, bsm.md_value;

update stemformatics.stats_datasetsummary set md_name= 'Sample Type' where md_name = 'Sample type long';

